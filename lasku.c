
//funktioita jotka palauttavat mitkä kolmiot tulisi piirtää.

struct d2kolmiolista
{
	SDL_Point (*pisteet)[4];
	int maara;
};

int _sijoita_y_x();

int _sijoita_y_z();


struct d2kolmiolista _kaikkikolmiot(struct maailma * maa)
//luo d2kolmiolistan,(dynaamisesti pisteet) jossa on kaikki kolmiot huonolla projektiolla.
{
	struct d2kolmiolista lista;
	struct objekti *obj = NULL;
	struct taso * tas = NULL;
	int i, j, kolmiomaara;

	lista.pisteet = NULL;
	lista.maara = 0;

	for(kolmiomaara = 0, i = 0; i < maa->maara; i++) //kolmiomaara kuvastaa kuinka mones kolmio on menossa d2kolmiolistassa
	{
		obj = get_objekti(maa, i);
		lista.maara += obj->maara;		//listaan tulee lisää kolmioita sen verran kuin objektissa on niitä
		lista.pisteet = realloc(lista.pisteet, sizeof(SDL_Point [3]) * lista.maara);

		for(j = 0; j < obj->maara; j++, kolmiomaara++)
		{
			tas = obj->tasot;
			lista.pisteet[kolmiomaara][0] = _huono_projektio((tas + j)->pisteet[0]);
			lista.pisteet[kolmiomaara][1] = _huono_projektio((tas + j)->pisteet[1]);
			lista.pisteet[kolmiomaara][2] = _huono_projektio((tas + j)->pisteet[2]);
		}
	}
	return lista;
}

struct d2kolmiolista _kaikkikolmiot2(struct maailma * maa, struct pelaaja *pel)
//polttopiste on nyt {0,0,0} ja suunta {0,0,1}
//luo d2kolmiolistan,(dynaamisesti pisteet) jossa on kaikki kolmiot huonolla projektiolla.
{
	struct piste suunta = {0, 0, 1}; //kamera
	struct d2kolmiolista lista;
	struct objekti *obj = NULL;
	struct taso * tas = NULL;
	int i, j, kolmiomaara;

	lista.pisteet = NULL;
	lista.maara = 0;

	for(kolmiomaara = 0, i = 0; i < maa->maara; i++) //kolmiomaara kuvastaa kuinka mones kolmio on menossa d2kolmiolistassa
	{
		obj = get_objekti(maa, i);
		lista.maara += obj->maara;		//listaan tulee lisää kolmioita sen verran kuin objektissa on niitä
		lista.pisteet = realloc(lista.pisteet, sizeof(SDL_Point [4]) * lista.maara);

		for(j = 0; j < obj->maara; j++, kolmiomaara++)
		{
			tas = obj->tasot;
			if(PISTETULO((tas + j)->pisteet[0], pel->n) < PISTETULO(pel->n, pel->sijainti) || PISTETULO((tas + j)->pisteet[1], pel->n) < PISTETULO(pel->n, pel->sijainti) || PISTETULO((tas + j)->pisteet[2], pel->n) < PISTETULO(pel->n, pel->sijainti))
			{ //tämä on sama kuin ylempi funktio, paitsi että suuntavektorin kanssa
				//kohtisuorassa olevat vektorit/pisteet jätetään piirtämättä
				lista.maara--;
				kolmiomaara--;
				continue;
			}

			lista.pisteet[kolmiomaara][0] = _huono_projektio_p(pel, (tas + j)->pisteet[0]);
			lista.pisteet[kolmiomaara][1] = _huono_projektio_p(pel, (tas + j)->pisteet[1]);
			lista.pisteet[kolmiomaara][2] = _huono_projektio_p(pel, (tas + j)->pisteet[2]);
			lista.pisteet[kolmiomaara][3].x = tas -> vari;
		}
	}
	return lista;
}


struct d2kolmiolista _kaikkikolmiot3(struct maailma * maa, struct pelaaja *pel)
{
	//kaikki polttpisteen takana olevat kolmiot jätetään laskematta ja piirtämättä
	//sijainti ja polttopiste pelaajassa muuttujassa c = camera
	struct d2kolmiolista lista;
	struct objekti *obj = NULL;
	struct taso * tas = NULL;
	int i, j, kolmiomaara;
	struct taso apu;
	lista.pisteet = NULL;
	lista.maara = 0;

	for(kolmiomaara = 0, i = 0; i < maa->maara; i++) //kolmiomaara kuvastaa kuinka mones kolmio on menossa d2kolmiolistassa
	{
		obj = get_objekti(maa, i);
		lista.maara += obj->maara;		//listaan tulee lisää kolmioita sen verran kuin objektissa on niitä
		lista.pisteet = realloc(lista.pisteet, sizeof(SDL_Point [4]) * lista.maara);

		for(j = 0; j < obj->maara; j++, kolmiomaara++)
		{
			tas = obj->tasot;
			if(PISTETULO((tas + j)->pisteet[0], pel->n) < PISTETULO(pel->n, pel->sijainti) || PISTETULO((tas + j)->pisteet[1], pel->n) < PISTETULO(pel->n, pel->sijainti) || PISTETULO((tas + j)->pisteet[2], pel->n) < PISTETULO(pel->n, pel->sijainti))
			{ //tämä on sama kuin ylempi funktio, paitsi että suuntavektorin kanssa
				//kohtisuorassa olevat vektorit/pisteet jätetään piirtämättä
				lista.maara--;
				kolmiomaara--;
				continue;
			}


			if(((tas+j) -> pisteet[0].y < pel -> c.y)||((tas+j) -> pisteet[1].y < pel -> c.y)||((tas+j) -> pisteet[2].y < pel -> c.y))
			{
				lista.maara--;
				kolmiomaara--;
				continue;
			}
			/*if(((tas+j) -> pisteet[0].z > pel -> c.z)||((tas+j) -> pisteet[1].z > pel -> c.z)||((tas+j) -> pisteet[2].z > pel -> c.z))
			{
				lista.maara--;
				kolmiomaara--;
				continue;
			}

*/



			/*struct piste apu = pel -> c;
			apu.x = apu.x - 450;
			apu.y = apu.y + pel -> polttopiste;

			//jos kaikki kolmion pisteet ovat näkökentän ulkopuolella, kolmiota ei piirretä
			//ruudun leveys on 900 px ja korkeus 700 px

			if((tas+j) -> pisteet[0].x < sijoita_y_x((tas+j) -> pisteet[0].y, pel -> c, apu)&&(tas+j) -> pisteet[1].x < sijoita_y_x((tas+j) -> pisteet[1].y, pel -> c, apu)&&(tas+j) -> pisteet[2].x < sijoita_y_x((tas+j) -> pisteet[2].y, pel -> c,apu))
			{
				lista.maara--;
				kolmiomaara--;
				continue;

				
			}

			apu.x = apu.x + 900;

			if((tas+j) -> pisteet[0].x > sijoita_y_x((tas+j) -> pisteet[0].y, pel -> c, apu)&&(tas+j) -> pisteet[1].x > sijoita_y_x((tas+j) -> pisteet[1].y, pel -> c, apu)&&(tas+j) -> pisteet[2].x > sijoita_y_x((tas+j) -> pisteet[2].y, pel -> c,apu))
			{
				lista.maara--;
				kolmiomaara--;
				continue;

				
			}
			
			apu.x = apu.x - 450;
			apu.z = apu.z - 350;

			if((tas+j) -> pisteet[0].z < sijoita_y_z((tas+j) -> pisteet[0].y, pel -> c, apu)&&(tas+j) -> pisteet[1].z < sijoita_y_z((tas+j) -> pisteet[1].y, pel -> c, apu)&&(tas+j) -> pisteet[2].z < sijoita_y_z((tas+j) -> pisteet[2].y, pel -> c,apu))
			{
				lista.maara--;
				kolmiomaara--;
				continue;

				
			}

			apu.z = apu.z + 700;
			if((tas+j) -> pisteet[0].z > sijoita_y_z((tas+j) -> pisteet[0].y, pel -> c, apu)&&(tas+j) -> pisteet[1].z > sijoita_y_z((tas+j) -> pisteet[1].y, pel -> c, apu)&&(tas+j) -> pisteet[2].z > sijoita_y_z((tas+j) -> pisteet[2].y, pel -> c,apu))
			{
				lista.maara--;
				kolmiomaara--;
				continue;

				
			}*/
			lista.pisteet[kolmiomaara][0] = _huono_projektio_p(pel, (tas + j)->pisteet[0]);
			lista.pisteet[kolmiomaara][1] = _huono_projektio_p(pel, (tas + j)->pisteet[1]);
			lista.pisteet[kolmiomaara][2] = _huono_projektio_p(pel, (tas + j)->pisteet[2]);
			lista.pisteet[kolmiomaara][3].x = tas -> vari;
		}
	}
	return lista;





}

SDL_Point katkaisekolmionviiva(SDL_Point ulkop, SDL_Point sisap)
{
	double k; //kulmakerroin, d(ulkop.x , sisap.x) on ehkä nolla, joten vielä ei lasketa
	double k2;
	int drop = 0;	//jos tapaus on case 5-8, niin ne voidaan käsitellä 1-4 avulla.
	SDL_Point palautettava;
	if(ulkop.x < 0 && ulkop.y < 0)		//case5
	{
		k = (ulkop.y - sisap.y + 0.0)/(ulkop.x - sisap.x + 0.0);
		k2 = (ulkop.y + 0.0)/(ulkop.x + 0.0);
		if(k == k2)
		{
			return (SDL_Point){0, 0};
		}
		if(k < k2)	//case3
		{
			drop = 3;
		}
		else		//case1
		{
			drop = 1;
		}
	}
	if(ulkop.x < 0 && ulkop.y >= 700)	//case6
	{
		k = (ulkop.y - sisap.y + 0.0)/(ulkop.x - sisap.x + 0.0);
		k2 = (ulkop.y - 699.0)/(ulkop.x - 0.0);
		if(k == k2)
		{
			return (SDL_Point){0, 699};
		}
		if(k < k2)	//case1
		{
			drop = 1;
		}
		else		//case4
		{
			drop = 4;
		}
	}
	if(ulkop.x >= 900 && ulkop.y < 0)	//case7
	{
		k = (ulkop.y - sisap.y + 0.0)/(ulkop.x - sisap.x + 0.0);
		k2 = (ulkop.y - 0.0)/(ulkop.x - 899.0);
		if(k == k2)
		{
			return (SDL_Point){899, 0};
		}
		if(k < k2)	//case2
		{
			drop = 2;
		}
		else		//case3
		{
			drop = 3;
		}
	}
	if(ulkop.x >= 900 && ulkop.y >= 700)	//case8
	{
		k = (ulkop.y - sisap.y + 0.0)/(ulkop.x - sisap.x + 0.0);
		k2 = (ulkop.y - 699.0)/(ulkop.x - 899.0);
		//printf("%f\n%f\n",k,k2);
		if(k == k2)
		{
			return (SDL_Point){899, 699};
		}
		if(k < k2)	//case4
		{
			drop = 4;
		}
		else		//case2
		{
			drop = 2;
		}
	}
	if((drop == 0 && ulkop.x < 0) || drop == 1)				//case1
	{
		k = (ulkop.y - sisap.y + 0.0)/(ulkop.x - sisap.x + 0.0);
		palautettava.x = 0;
		palautettava.y = (int)round(-k * ulkop.x + ulkop.y);
		return palautettava;
	}
	if((drop == 0 && ulkop.x >= 900) || drop == 2)			//case2
	{
		k = (ulkop.y - sisap.y + 0.0)/(ulkop.x - sisap.x + 0.0);
		palautettava.x = 899;
		palautettava.y = (int)round(k * 899 -k * ulkop.x + ulkop.y);
		return palautettava;
	}
	if((drop == 0 && ulkop.y < 0) || drop == 3)				//case3
	{
		if(ulkop.x == sisap.x)
		{
			palautettava.x = ulkop.x;
			palautettava.y = 0;
			return palautettava;
		}
		k = (ulkop.y - sisap.y + 0.0)/(ulkop.x - sisap.x + 0.0);
		palautettava.y = 0;
		palautettava.x = (int)round((k * ulkop.x - ulkop.y + 0.0) / k);
		return palautettava;
	}
	if((drop == 0 && ulkop.y >= 700) || drop == 4)			//case4
	{
		if(ulkop.x == sisap.x)
		{
			palautettava.x = ulkop.x;
			palautettava.y = 699;
			return palautettava;
		}
		k = (ulkop.y - sisap.y + 0.0)/(ulkop.x - sisap.x + 0.0);
		palautettava.y = 699;
		palautettava.x = (int)round((699 + k * ulkop.x - ulkop.y + 0.0) / k);
		return palautettava;
	}
}

struct d2kolmiolista kaikkikolmiot4(struct maailma * maa, struct pelaaja2 *pel)
{
	struct d2kolmiolista lista;
	struct objekti *obj = NULL;
	struct taso * tas = NULL;
	int i, j, k, kolmiomaara;

	SDL_Point apupisteet[4];

	lista.pisteet = NULL;
	lista.maara = 0;

	for(kolmiomaara = 0, i = 0; i < maa->maara; i++) //kolmiomaara kuvastaa kuinka mones kolmio on menossa d2kolmiolistassa
	{
		obj = get_objekti(maa, i);
		lista.maara += obj->maara;		//listaan tulee lisää kolmioita sen verran kuin objektissa on niitä
		lista.pisteet = realloc(lista.pisteet, sizeof(SDL_Point [4]) * lista.maara);

		for(j = 0; j < obj->maara; j++, kolmiomaara++)
		{
			tas = obj->tasot;
			int broke = 0;
			for(k = 0; k < 3; k++) //tarkistetaan kohtisuoruus ja onko piste kameran takana.
			{
				struct pisted d = kaanna_koordinaatistoa((tas + j)->pisteet[k], pel->c, pel->f);
				if(d.z >= 0)
				{
					broke = 1;
					break;
				}
			}
			if(broke == 1)
			{
				lista.maara--;
				kolmiomaara--;
				continue;
			}
			apupisteet[0] = huono_projektio_p2(pel, (tas + j)->pisteet[0]);
			apupisteet[1] = huono_projektio_p2(pel, (tas + j)->pisteet[1]);
			apupisteet[2] = huono_projektio_p2(pel, (tas + j)->pisteet[2]);
			apupisteet[3].x = tas -> vari;


			/*broke = 0;	//tässä tapauksessa jos kaikki pisteet ovat kameran ulkopuolella, niin mitään ei piirretä
			for(k = 0; k < 3; k++) // bugi, jos kaikki pisteet ovat ulkopuolella, kolmiosta voi siltikin näkyä jotain.
			{
				if(!(apupisteet[k].x < 0 ||
				apupisteet[k].x >= 900 ||
				apupisteet[k].y < 0 ||
				apupisteet[k].y >= 700))
				{
					broke = 1;
					break;
				}
			}
			if(broke == 0)
			{
				lista.maara--;
				kolmiomaara--;
				continue;
			}*/
			//triangle clipping(?) eli piirrettävät kolmiot leikataan näyttöön sopiviksi
			int ulkop = 0;
			int ulkona = 0;
			//ulkona sanoo mitkä pisteet ovat ulkopuolella bitteinä.
			for(k = 0; k < 3; k++)
			{
				if(apupisteet[k].x < 0 ||
				apupisteet[k].x >= 900 ||
				apupisteet[k].y < 0 ||
				apupisteet[k].y >= 700)
				{
					ulkop++;
					if(k == 0)
						ulkona |= 1;
					if(k == 1)
						ulkona |= 2;
					if(k == 2)
						ulkona |= 4;
				}
			}
			SDL_Point apupisteet2[5] = {{0, 0},{0, 0},{0, 0},{0, 0},{0, 0}};
			if(ulkop == 0)
			{
				lista.pisteet[kolmiomaara][0] = apupisteet[0];
				lista.pisteet[kolmiomaara][1] = apupisteet[1];
				lista.pisteet[kolmiomaara][2] = apupisteet[2];
				lista.pisteet[kolmiomaara][3].x = tas -> vari; //väri
			}
			else if(ulkop == 1)	//0-1 kolmiota lisää, yht 1-2 piirrettäväksi
			{
				int montapistetta = 4;
				if((ulkona & 1) == 1) //1. piste
				{
					apupisteet2[0] = apupisteet[1];
					apupisteet2[1] = apupisteet[2];
					apupisteet2[2] = katkaisekolmionviiva(apupisteet[0], apupisteet[1]);
					apupisteet2[3] = katkaisekolmionviiva(apupisteet[0], apupisteet[2]);
				}
				else if((ulkona & 2) == 2) //2. piste
				{
					apupisteet2[0] = apupisteet[0];
					apupisteet2[1] = apupisteet[2];
					apupisteet2[2] = katkaisekolmionviiva(apupisteet[1], apupisteet[0]);
					apupisteet2[3] = katkaisekolmionviiva(apupisteet[1], apupisteet[2]);
				}
				else if((ulkona & 4) == 4) //3. piste
				{
					apupisteet2[0] = apupisteet[0];
					apupisteet2[1] = apupisteet[1];
					apupisteet2[2] = katkaisekolmionviiva(apupisteet[2], apupisteet[0]);
					apupisteet2[3] = katkaisekolmionviiva(apupisteet[2], apupisteet[1]);
				}
				else printf("Virhe: %d\n%d\n%d\n", ulkona, ulkona & 2, ulkona &4);
				if(apupisteet2[2].x != apupisteet2[3].x && apupisteet2[2].y != apupisteet2[3].y)
				{
					if(apupisteet2[2].x == 0 || apupisteet2[3].x == 0)
						apupisteet2[4].x = 0;
					else
						apupisteet2[4].x = 899;
					if(apupisteet2[2].y == 0 || apupisteet2[3].y == 0)
						apupisteet2[4].y = 0;
					else
						apupisteet2[4].y = 699;
					montapistetta++;
				}
				//jaetaan kolmioihin:
				lista.pisteet[kolmiomaara][0] = apupisteet2[0];
				lista.pisteet[kolmiomaara][1] = apupisteet2[1];
				lista.pisteet[kolmiomaara][2] = apupisteet2[2];
				lista.pisteet[kolmiomaara][3].x = tas->vari; //väri
				lista.maara++;	
				lista.pisteet = realloc(lista.pisteet, sizeof(SDL_Point [4]) * lista.maara);
				kolmiomaara++;

				lista.pisteet[kolmiomaara][0] = apupisteet2[1];
				lista.pisteet[kolmiomaara][1] = apupisteet2[2];
				lista.pisteet[kolmiomaara][2] = apupisteet2[3];
				lista.pisteet[kolmiomaara][3].x = tas->vari; //väri
				if(montapistetta != 5)
					continue;
				//kolmas kolmio jos tarvitaan
				lista.maara++;
				lista.pisteet = realloc(lista.pisteet, sizeof(SDL_Point [4]) * lista.maara);
				kolmiomaara++;
				lista.pisteet[kolmiomaara][0] = apupisteet2[2];
				lista.pisteet[kolmiomaara][1] = apupisteet2[3];
				lista.pisteet[kolmiomaara][2] = apupisteet2[4];
				lista.pisteet[kolmiomaara][3].x = tas->vari; //väri
				
			}
			else if(ulkop == 2)
			{
				int montapistetta = 3; //voi olla 3,4,5
				if((ulkona & 1) != 1) // eli sisällä 1 , [0].
				{
					apupisteet2[0] = apupisteet[0];
					apupisteet2[1] = katkaisekolmionviiva(apupisteet[1], apupisteet[0]); //ulko, sisä
					apupisteet2[2] = katkaisekolmionviiva(apupisteet[2], apupisteet[0]); //ulko, sisä

					SDL_Point extra_apu = katkaisekolmionviiva(apupisteet[1], apupisteet[2]);
					if(extra_apu.x >= 0 && extra_apu.x < 900 && extra_apu.y >= 0 && extra_apu.y < 700)//siis näytön sisällä
					{//jolloin pisteitä on 5
						montapistetta = 5;
						apupisteet2[3] = katkaisekolmionviiva(apupisteet[1], apupisteet[2]);
						apupisteet2[4] = katkaisekolmionviiva(apupisteet[2], apupisteet[1]);
					}
				}
				else if((ulkona & 2) != 2) // eli sisällä 2 , [1].
				{
					apupisteet2[0] = apupisteet[1];
					apupisteet2[1] = katkaisekolmionviiva(apupisteet[0], apupisteet[1]); //ulko, sisä
					apupisteet2[2] = katkaisekolmionviiva(apupisteet[2], apupisteet[1]); //ulko, sisä

					SDL_Point extra_apu = katkaisekolmionviiva(apupisteet[0], apupisteet[2]);
					if(extra_apu.x >= 0 && extra_apu.x < 900 && extra_apu.y >= 0 && extra_apu.y < 700)//siis näytön sisällä
					{//jolloin pisteitä on 5
						montapistetta = 5;
						apupisteet2[3] = katkaisekolmionviiva(apupisteet[0], apupisteet[2]);
						apupisteet2[4] = katkaisekolmionviiva(apupisteet[2], apupisteet[0]);
					}
				}
				else if((ulkona & 4) != 2) // eli sisällä 3 , [2].
				{
					apupisteet2[0] = apupisteet[2];
					apupisteet2[1] = katkaisekolmionviiva(apupisteet[0], apupisteet[2]); //ulko, sisä
					apupisteet2[2] = katkaisekolmionviiva(apupisteet[1], apupisteet[2]); //ulko, sisä

					SDL_Point extra_apu = katkaisekolmionviiva(apupisteet[0], apupisteet[1]);
					if(extra_apu.x >= 0 && extra_apu.x < 900 && extra_apu.y >= 0 && extra_apu.y < 700)//siis näytön sisällä
					{//jolloin pisteitä on 5
						montapistetta = 5;
						apupisteet2[3] = katkaisekolmionviiva(apupisteet[0], apupisteet[1]);
						apupisteet2[4] = katkaisekolmionviiva(apupisteet[1], apupisteet[0]);
					}
				}

				//jos montapistetta == 3
				if(montapistetta == 3 && (apupisteet2[1].x != apupisteet2[2].x && apupisteet2[1].y != apupisteet2[2].y))
				{
					if(apupisteet2[1].x == 0 || apupisteet2[2].x == 0)
						apupisteet2[3].x = 0;
					else
						apupisteet2[3].x = 899;
					if(apupisteet2[1].y == 0 || apupisteet2[2].y == 0)
						apupisteet2[3].y = 0;
					else
						apupisteet2[3].y = 699;
					montapistetta++;
				}
				//jaetaan kolmioihin
				lista.pisteet[kolmiomaara][0] = apupisteet2[0];
				lista.pisteet[kolmiomaara][1] = apupisteet2[1];
				lista.pisteet[kolmiomaara][2] = apupisteet2[2];
				lista.pisteet[kolmiomaara][3].x = tas->vari; //väri
				if(montapistetta == 3)
					continue;

				lista.maara++;
				lista.pisteet = realloc(lista.pisteet, sizeof(SDL_Point [4]) * lista.maara);
				kolmiomaara++;

				lista.pisteet[kolmiomaara][0] = apupisteet2[1]; // 1
				lista.pisteet[kolmiomaara][1] = apupisteet2[2]; // 2
				lista.pisteet[kolmiomaara][2] = apupisteet2[3]; // 3
				lista.pisteet[kolmiomaara][3].x = tas->vari; //väri

				if(montapistetta == 4)
					continue;

				lista.maara++;
				lista.pisteet = realloc(lista.pisteet, sizeof(SDL_Point [4]) * lista.maara);
				kolmiomaara++;

				lista.pisteet[kolmiomaara][0] = apupisteet2[1]; // 2
				lista.pisteet[kolmiomaara][1] = apupisteet2[3]; // 3
				lista.pisteet[kolmiomaara][2] = apupisteet2[4]; // 4
				lista.pisteet[kolmiomaara][3].x = tas->vari; //väri

			}
			else
			{
				lista.maara--;
				kolmiomaara--;
			}
		}
	}
	return lista;
}

int _sijoita_y_x(int y_sijoitettava, struct piste b1, struct piste b2) //palauttaa x
{
    if (b1.x == b2.x)
    {
        return b1.x;
    }
    double k =  ((double)b1.y - (double)b2.y)/((double)b1.x - (double)b2.x); //kulmakerroin
    double c = (double)b1.y -( k * (double)b1.x); //vakio
    double x = ((double)y_sijoitettava - c)/k;
    return (int)x;
}

int _sijoita_y_z(int y_sijoitettava, struct piste b1, struct piste b2) //palauttaa x
{
    if (b1.z == b2.z)
    {
        return b1.z;
    }
    double k =  ((double)b1.y - (double)b2.y)/((double)b1.z - (double)b2.z); //kulmakerroin
    double c = (double)b1.y -( k * (double)b1.z); //vakio
    double z = ((double)y_sijoitettava - c)/k;
    return (int)z;
}
double _keskiarvo_objektille(struct objekti * obj, struct pelaaja2 * pel)
{
	int apux;
	int apuy;
	int apuz;
	int pistemaara;

	for(int i = 0; i < obj -> maara; i++)
	{
		apux = obj->tasot[i].pisteet->x;
		apuy = obj->tasot[i].pisteet->y;
		apuz = obj->tasot[i].pisteet->z;
		pistemaara++;
	}
	

	double loppux = pow((pel ->c.x) - (apux/pistemaara), 2.0);
	double loppuy = pow((pel ->c.y) - (apuy/pistemaara), 2.0);
	double loppuz = pow((pel ->c.z) - (apuz/pistemaara), 2.0);

	return sqrt(loppux + loppuy + loppuz);

}
/*
*
*
*limitysvaihe
*/
void _merge_in_world(struct maailma * maa, struct pelaaja2 * pel, int alkualkio, int keskikohta, int loppualkio)
{
	int pituus1 = keskikohta-alkualkio + 1;
	int pituus2 = loppualkio - keskikohta;
	//apuvektorit
	struct maailma * apu1 = malloc(sizeof(maa -> objektit[0])*(pituus1 + 1));
	struct maailma * apu2 = malloc(sizeof(maa -> objektit[0])*(pituus2 + 1));

	for(int i = 0; i<pituus1; i++)
	{
		lisaa_objekti2(apu1,maa ->objektit[alkualkio + i - 2],0,0,0,0);
	}

	for(int i = 0; i<pituus2; i++)
	{
		lisaa_objekti2(apu2, maa ->objektit[keskikohta + i - 1],0,0,0,0);
	}
	//pysäytysalkiot
	lisaa_objekti(apu1, _uusi_kuutio,0.0,0,0,0);
	lisaa_objekti(apu2, _uusi_kuutio,0.0,0,0,0);

	int i = 0;
	int j = 0;

	for(int k = alkualkio; k < loppualkio + 1; k++)
	{
		double keskiarvo;
		//vertaa objektien kolmioiden pisteiden keskiarvoja
		if(get_objekti(apu2, j) ->nimi == "kuutio\0"||_keskiarvo_objektille(get_objekti(apu1, i), pel)<_keskiarvo_objektille(get_objekti(apu2,j), pel))
		{
			maa -> objektit[k] = get_objekti(apu1, i);
			i++;
			keskiarvo = _keskiarvo_objektille(get_objekti(apu1, i), pel);
		}
		else if(get_objekti(apu1, i) ->nimi == "kuutio\0"||_keskiarvo_objektille(get_objekti(apu1, i), pel)>_keskiarvo_objektille(get_objekti(apu2,j), pel))
		{
			maa -> objektit[k] = get_objekti(apu2, j);
			j++;
			keskiarvo = _keskiarvo_objektille(get_objekti(apu2, j), pel);
		}
		else
		{
			maa -> objektit[k] = get_objekti(apu1, i);
			i++;
			keskiarvo =_keskiarvo_objektille(get_objekti(apu1, i), pel);
		}
		//printf("limitetty objekti, jonka painoarvo on %f\n", keskiarvo);
	}
	free(apu1);
	free(apu2);

}
/*
*
*
järjestää maailman kolmiot piirto/projektiojärjestykseen limityslajittelulla*/
void _mergesort_in_world(struct maailma * maa, struct pelaaja2 * pel, int alkualkio, int loppualkio)
{
	int alku = alkualkio;
	int loppu = loppualkio;

	if(alku<loppu)
	{
		int keski = (int) floor(((double) alku + (double) loppu)/2);
		_mergesort_in_world(maa, pel, alku, keski);
		_mergesort_in_world(maa, pel, keski, loppu);
		_merge_in_world(maa, pel, alku, keski, loppu);
	}
}
void _vaihda(struct maailma * maa,  int o1, int o2)
{
	struct objekti * apu = get_objekti(maa, o1);
	maa->objektit[o1] = get_objekti(maa, o2);
	maa->objektit[o2] = apu;
	printf("vaihdettu %d ja %d\n", o1, o2);
}
void _swap_sort(struct maailma * maa, struct pelaaja2 * pel)
{
	int i;
	int j;
	int pituus = maa -> maara;
	for(i = 0; i < pituus - 1; i++)
	{
		int min = i;

		for(j = i + 1;  j < pituus; j++)
		{
			if(_keskiarvo_objektille(get_objekti(maa, j),pel) < _keskiarvo_objektille(get_objekti(maa, min), pel))
			{

				min = j;
			}
		}
		if (min != i)
		{
			_vaihda(maa,  i, min);
		}
	}

}


/*
void merge_in_object(struct objekti * obj, struct pelaaja2 * pel, int alkualkio, int keskikohta, int loppualkio)
{

}
void mergesort_in_object(struct objekti * obj, struct pelaaja2 * pel, int alkualkio, int loppualkio)
{

}*/
struct d2kolmiolista laske_maailma(struct maailma * maa, struct pelaaja2 *pel)
{
	//swap_sort(maa,pel);
	return kaikkikolmiot4(maa, pel);
}
