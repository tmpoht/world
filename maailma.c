#include <time.h>
//#include "objektit.c"
// maailma joko kovakoodataan tai ladataan tiedostosta.
// maailmassa on kaikki objektit.

struct maailma
{
	struct objekti **objektit; // lista objekteista
	int maara;
};

void lisaa_objekti(struct maailma * maa, struct objekti * (*p)(int), int d, int x, int y, int z)
//lisaa objektin maailmaan reallocatella, objekti on pointerilla funktioon luotu
{
	maa->maara++;
	maa->objektit = realloc(maa->objektit, sizeof(struct objekti *) * maa->maara);
	*(maa->objektit + maa->maara - 1) = p(d);
	siirra_objektia(*(maa->objektit + maa->maara - 1), x, y, z);
}
void lisaa_objekti2(struct maailma * maa, struct objekti * oma, double d, int x, int y, int z)
//lisaa objektin maailmaan reallocatella, objekti on pointerilla funktioon luotu
{
	maa->maara++;
	maa->objektit = realloc(maa->objektit, sizeof(struct objekti *) * maa->maara);
	*(maa->objektit + maa->maara - 1) = oma;
	muuta_objektin_kokoa(*(maa->objektit + maa->maara - 1), d);
	siirra_objektia(*(maa->objektit + maa->maara - 1), x, y, z);
}

struct maailma * _luo_maailma1() // luo yksinkertaisen maailman jossa on 1 kuutio
{
	struct maailma * maa = (struct maailma *)malloc(sizeof(struct maailma));
	maa->maara = 1;
	maa->objektit = malloc(sizeof(struct objekti *) * maa->maara);
	*(maa->objektit) = _uusi_kuutio(10);
	siirra_objektia(*(maa->objektit), 100, 100, 0);
	return maa;
}

struct maailma * _luo_maailma2() // luo yksinkertaisen maailman jossa on kuutioita
{
	struct maailma * maa = (struct maailma *)malloc(sizeof(struct maailma));
	maa->objektit = NULL;
	maa->maara = 0;

	lisaa_objekti(maa, _uusi_kuutio, 100, 100, 100, 100);
	lisaa_objekti(maa, _uusi_kuutio, 100, 100, 120, 0);
	lisaa_objekti(maa, _uusi_kuutio, 200, 300, 320, 0);
	lisaa_objekti(maa, _uusi_kuutio, 10, 445, 345, 0);

	lisaa_objekti(maa, _uusi_kuutio, 100, 100, 400, 0);
	lisaa_objekti(maa, _uusi_kuutio, 100, 100, 400, 200);
	lisaa_objekti(maa, _uusi_kuutio, 100, 100, 400, 400);

	return maa;
}

struct maailma * _luo_maailma3()
{
	struct maailma * maa = malloc(sizeof(struct maailma));
	maa->objektit = NULL;
	maa->maara = 0;

	lisaa_objekti(maa, _uusi_kuutio, 100, 10, 10, 10);


	return maa;
}
struct maailma * _luo_maailma4()
{
	struct maailma * maa = malloc(sizeof(struct maailma));
	maa->objektit = NULL;
	maa->maara = 0;

	
	for(int j = -30; j<30 ; j++)
	{
		
		for(int i = 0; i < 60; i++)
		{
			if (i % 5 == 0 && j % 5 == 0)
			{
				lisaa_objekti2(maa, uusi_tiedostosta("drawings/stone.stl",0x999999), 100, j*36+10, i*36+10, 0);
			}
			
			lisaa_objekti2(maa, uusi_tiedostosta("drawings/grass.stl",0x00ff00), 100, j*36, i*36, 0);
		}
		
	}
	
	lisaa_objekti2(maa, uusi_tiedostosta("drawings/runko.stl",0xaa0000), 100, 0, 0, 0);
	lisaa_objekti2(maa, uusi_tiedostosta("drawings/latva.stl",0x00ff00), 100, 0, 0, 0);

	lisaa_objekti2(maa, uusi_tiedostosta("drawings/runko.stl",0xaa0000), 100, 100, 100, 0);
	lisaa_objekti2(maa, uusi_tiedostosta("drawings/latva.stl",0x00ff00), 100, 100, 100, 0);
	
	lisaa_objekti2(maa, uusi_tiedostosta("drawings/runko.stl",0xaa0000), 100, -500, 200, 0);
	lisaa_objekti2(maa, uusi_tiedostosta("drawings/latva.stl",0x00ff00), 100, -500, 200, 0);
	
	lisaa_objekti2(maa, uusi_tiedostosta("drawings/runko.stl",0xaa0000), 100, 1000, -300, 0);
	lisaa_objekti2(maa, uusi_tiedostosta("drawings/latva.stl",0x00ff00), 100, 1000, -300, 0);
	
	lisaa_objekti2(maa, uusi_tiedostosta("drawings/runko.stl",0xaa0000), 100, 700, 0, 0);
	lisaa_objekti2(maa, uusi_tiedostosta("drawings/latva.stl",0x00ff00), 100, 700, 0, 0);
	
	lisaa_objekti2(maa, uusi_tiedostosta("drawings/runko.stl",0xaa0000), 100, -100, -700, 0);
	lisaa_objekti2(maa, uusi_tiedostosta("drawings/latva.stl",0x00ff00), 100, -100, -700, 0);





	return maa;
}

struct maailma * _luo_maailma5()
{
	struct maailma * maa = malloc(sizeof(struct maailma));
	maa->objektit = NULL;
	maa->maara = 0;
	srand(time(0));

	//lisaa_objekti(maa, uusi_kuutio, 1500, -250, -250, -1500);

	lisaa_objekti2(maa, uusi_tiedostosta("drawings/land1.stl",0x30d030), 1, 0, 0, 0);
	lisaa_objekti2(maa, uusi_tiedostosta("drawings/land1.stl",0x30d030), 1, 0, 1000, 0);
	lisaa_objekti2(maa, uusi_tiedostosta("drawings/land1.stl",0x30d030), 1, 1000, 0, 0);
	lisaa_objekti2(maa, uusi_tiedostosta("drawings/land1.stl",0x30d030), 1, 1000, 1000, 0);

	lisaa_objekti2(maa, uusi_tiedostosta("drawings/house.stl",0xcc0000), 1, 900, 900, 0);
	lisaa_objekti2(maa, uusi_tiedostosta("drawings/roof.stl",0x00ffff), 1, 900, 900, 0);
	

	for (int i = 0; i < 75; i++)
	{
		
		int r1 = rand() % 1000;
		int r2 = rand() % 1000;

		lisaa_objekti2(maa, uusi_tiedostosta("drawings/grass.stl",0x00ff00), 1, r1, r2, 0);

	}



	for (int i = 0; i < 35; i++)
	{
		
		int r1 = rand() % 1000;
		int r2 = rand() % 1000;
		
		lisaa_objekti2(maa, uusi_tiedostosta("drawings/stone.stl",0x999999), 1, r1, r2, 0);
		
	}
	for (int i =0;i < 15; i++)
	{
		
		int r1 = rand() % 1000;
		int r2 = rand() % 1000;

		lisaa_objekti2(maa, uusi_tiedostosta("drawings/runko.stl",0xaa0000), 1, r1, r2, 0);
		lisaa_objekti2(maa, uusi_tiedostosta("drawings/latva.stl",0x00ff00), 1, r1, r2, 0);
		

	}
	return maa;
}

struct maailma * _luo_maailma6()
{
	struct maailma * maa = malloc(sizeof(struct maailma));
	maa->objektit = NULL;
	maa->maara = 0;
	srand(time(0));

	int i, j;
	int r1, r2, r3;
	double dr1;
	for(i = 0; i < 4; i++)
	{
		for(j = 0; j < 4; j++)
			lisaa_objekti(maa, uusi_slab, 250, i*250, j*250, 0);
	}

	for(i = 0; i < 20; i++)
	{
		r1 = rand() % 900;
		r2 = rand() % 900 + 50;
		dr1 = rand()/(RAND_MAX / 1.0) - 0.5;
		lisaa_objekti2(maa, uusi_tiedostosta("drawings/stone.stl",0x999999), dr1, r1, r2, 0);
	}

	lisaa_objekti2(maa, uusi_tiedostosta("drawings/kuusi_runko.stl",0x7C1919), 1, 950, 950, 0);
	lisaa_objekti2(maa, uusi_tiedostosta("drawings/kuusi_oksat.stl",0x11AC00), 1, 950, 950, 0);

	lisaa_objekti2(maa, uusi_tiedostosta("drawings/house.stl", 0x412F10), 1, 600, 600, 0);
	lisaa_objekti2(maa, uusi_tiedostosta("drawings/roof.stl", 0x0), 1, 600, 600, 0);

	for(i = 0; i < 2; i++)
	{
		r1 = 450 + rand() % 500;
		r2 = 50 + rand() % 500;
		r3 = rand() % 46; //46
		dr1 = rand()/(RAND_MAX / 1.0)+ 1.0;		//skalaari
		printf("%f\n",dr1);
		lisaa_objekti2(maa, uusi_tiedostosta("drawings/kuusi_runko.stl", 0x7C1919), dr1, r1, r2, 0);
		lisaa_objekti2(maa, uusi_tiedostosta("drawings/kuusi_oksat.stl",0x0D5013 + (r3 << 8)), dr1, r1, r2, 0);
	}

	for(i = 0; i < 2; i++)
	{
		r1 = 50 + rand() % 500;
		r2 = 450 + rand() % 500;
		r3 = rand() % 46; //46
		dr1 = rand()/(RAND_MAX / 1.0)+ 1.0;		//skalaari
		printf("%f\n",dr1);
		lisaa_objekti2(maa, uusi_tiedostosta("drawings/kuusi_runko.stl",0x7C1919), dr1, r1, r2, 0);
		lisaa_objekti2(maa, uusi_tiedostosta("drawings/kuusi_oksat.stl",0x0D5013 + (r3 << 8)), dr1, r1, r2, 0); //0x11AC00
	}

	for(i = 0; i < 3; i++)
	{
		r1 = 100 + rand() % 300;
		r2 = 100 + rand() % 300;
		r3 = rand() % 46; //46
		dr1 = rand()/(RAND_MAX / 1.0)+ 1.0;		//skalaari
		printf("%f\n",dr1);
		lisaa_objekti2(maa, uusi_tiedostosta("drawings/kuusi_runko.stl",0x7C1919), dr1, r1, r2, 0);
		lisaa_objekti2(maa, uusi_tiedostosta("drawings/kuusi_oksat.stl",0x0D5013 + (r3 << 8)), dr1, r1, r2, 0);
	}




	return maa;
}

struct maailma * luo_maailma7()
{
	struct maailma * maa = malloc(sizeof(struct maailma));
	maa->objektit = NULL;
	maa->maara = 0;
	srand(time(0));

	int i, j;
	int r1, r2, r3;
	double dr1;
	for(i = 0; i < 40; i++)
	{
		for(j = 0; j < 40; j++)
			lisaa_objekti(maa, uusi_slab, 250, i*250, j*250, 0);
	}

	for(i = 0; i < 300; i++)
	{
		r1 = rand() % (40 * 250);
		r2 = rand() % (40 * 250);
		dr1 = rand()/(RAND_MAX / 1.5) - 1.0;
		lisaa_objekti2(maa, uusi_tiedostosta("drawings/stone.stl",0x999999), dr1, r1, r2, 0);
	}

	//lisaa_objekti2(maa, uusi_tiedostosta("drawings/kuusi_runko.stl",0x7C1919), 1, 950, 950, 0);
	//lisaa_objekti2(maa, uusi_tiedostosta("drawings/kuusi_oksat.stl",0x11AC00), 1, 950, 950, 0);

	lisaa_objekti2(maa, uusi_tiedostosta("drawings/house.stl", 0x412F10), 1, 600, 600, 0);
	lisaa_objekti2(maa, uusi_tiedostosta("drawings/roof.stl", 0x0), 1, 600, 600, 0);

	for(i = 0; i < 200; i++)
	{
		r1 = rand() % (40 * 250);
		r2 = rand() % (40 * 250);
		r3 = rand() % 46; //46
		dr1 = rand()/(RAND_MAX / 1.0)+ 1.0;		//skalaari
		lisaa_objekti2(maa, uusi_tiedostosta("drawings/kuusi_runko.stl", 0x7C1919), dr1, r1, r2, 0);
		lisaa_objekti2(maa, uusi_tiedostosta("drawings/kuusi_oksat.stl",0x0D5013 + (r3 << 8)), dr1, r1, r2, 0);
	}




	return maa;
}


struct objekti * get_objekti(struct maailma * maa, int n)
{
	if(n > maa->maara)
		return NULL;
	return *(maa->objektit + n);
}
