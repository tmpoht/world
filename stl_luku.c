#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>

void tulosta_pisteet();

int main(int argc, char* args[])
{
    printf("testi\n");
    tulosta_pisteet("drawings/kuusi_oksat.stl");
}

struct kolmiodata
{
    float normaali[3];
    float kulma1[3];
    float kulma2[3];
    float kulma3[3];
    short attribuutti;
};

void tulosta_pisteet(char tiedostonimi[])
{
    FILE *fp = fopen(tiedostonimi,"rb");
    char otsake[80];
    unsigned int lkm;
    

    fread(&otsake, sizeof(otsake), 1, fp);
    fread(&lkm, sizeof(lkm),1,fp);

    struct kolmiodata kolmiot[lkm];
    for(int i = 0; i<lkm; i++)
    {
        fread(&(kolmiot[i].normaali), sizeof(kolmiot[i].normaali),1,fp);
        fread(&(kolmiot[i].kulma1), sizeof(kolmiot[i].kulma1),1,fp);
        fread(&(kolmiot[i].kulma2), sizeof(kolmiot[i].kulma2),1,fp);
        fread(&(kolmiot[i].kulma3), sizeof(kolmiot[i].kulma3),1,fp);
        fread(&(kolmiot[i].attribuutti), sizeof(kolmiot[i].attribuutti),1,fp);
    }

    fclose(fp);

    printf("%d\n",lkm);

    for (int j = 0; j<lkm; j++)
    {
        printf("%d: x = %f, y = %f, z = %f \n",j,kolmiot[j].kulma1[0],kolmiot[j].kulma1[1],kolmiot[j].kulma1[2]);
        printf("%d: x = %f, y = %f, z = %f \n",j,kolmiot[j].kulma2[0],kolmiot[j].kulma2[1],kolmiot[j].kulma2[2]);
        printf("%d: x = %f, y = %f, z = %f \n",j,kolmiot[j].kulma3[0],kolmiot[j].kulma3[1],kolmiot[j].kulma3[2]);
    }

}
