#include <math.h>
//Tämä tiedosto on pelaajaan liittyville funktioille ja tietorakenteille.
#define RISTITULO(i, n) {i.y*n.z - i.z*n.y, i.z*n.x - i.x*n.z, i.x*n.y - i.y*n.x}
#define PISTETULO(p1, p2) (p1.x * p2.x + p1.y * p2.y + p1.z * p2.z)

struct pelaaja
{
	struct piste sijainti;		//pelaajan sijainti, ei ole sama kuin kameran polttopiste, se olkoon pelaajan takana.
	int polttopiste;		//olkoon tämä sijainnista taaksepäin normaalivektorin suuntaisesti.
	double paankaantox;		//radiaaneissa
	double paankaantoy;
	double paankaantoz;


	//tämän kommentin alla olevia arvoja tulisi muuttaa tässä tiedostossa olevilla funktioilla
	//nämä arvot lasketaan mm. sijainnilla ja polttopisteellä sekä paankaatox,y.
	//vain näitä arvoja tarvitaan projektiossa
	struct piste c;			//kamera
	struct piste n;			//katsesuunta ja projektiossa tason normaalivektori
	struct pisted i;		//"pään kallistus", katso projektion kommentteja, tämä on projektion vektori i. Tämän on oltava tasossa.
	struct piste e;			//piste, josta taso alkaa. Tätä ei välttämättä tarvita vaan se voi olla sijaintiin kytköksissä
	struct pisted f;		//kameran kääntö (ei vielä olemassa tai tarkoitusta)

};

struct pelaaja2			//tämä on pelaaja paremmalle projektiolle
{
	struct pisted c;	//kamera
	struct pisted f;	//kameran suunta (x,y,z) radiaaneissa
};

struct pelaaja2 uusi_pelaaja2()
{
	struct pelaaja2 p = {{1000, 1000, 1000}, {0, 0, 0}};
	return p;
}


void pelaaja2_kaannos_paa_vasen(struct pelaaja2 *p, double d)
{
	p->f.z += M_PI/180.0 * d;
}

void pelaaja2_kaannos_paa_oikea(struct pelaaja2 *p, double d)
{
	p->f.z -= M_PI/180.0 * d;
}

void pelaaja2_kaannos_vasen(struct pelaaja2 *p, double d)
{
	p->f.y += M_PI/180.0 * d;
}

void pelaaja2_kaannos_oikea(struct pelaaja2 *p, double d)
{
	p->f.y -= M_PI/180.0 * d;
}

void pelaaja2_kaannos_ylos(struct pelaaja2 *p, double d)
{
	p->f.x += M_PI/180.0 * d;
}

void pelaaja2_kaannos_alas(struct pelaaja2 *p, double d)
{
	p->f.x -= M_PI/180.0 * d;
}

void pelaaja2_x(struct pelaaja2 *p, double d)
{
	p->c.x +=d;
}

void pelaaja2_y(struct pelaaja2 *p, double d)
{
	p->c.y +=d;
}

void pelaaja2_z(struct pelaaja2 *p, double d)
{
	p->c.z +=d;
}

struct pelaaja _uusi_pelaaja()
{
	struct pelaaja p = {{0, 0, 50}, 100, M_PI/2, 2*M_PI/5, 0, {0, 0, -100}, {0, 0, 1}, {0, 1, 0}, {-450, -350, 0}, {0, 0, 0}};
	return p;
}

void _laske_n(struct pelaaja *p)
{
	//vielä ei voi kääntää kameraa pystysuunnassa.
	//p->n.z = round((sin(p->paankaantox) * 100));
	//p->n.x = round((cos(p->paankaantox) * 100));

	p->n.x = round(cos(p->paankaantox) * cos(p->paankaantoy) * 100);
	p->n.z = round(sin(p->paankaantox) * cos(p->paankaantoy) * 100);
	p->n.y = round(sin(p->paankaantoy) * 100);


}

void _laske_c(struct pelaaja *p)
{
	//vielä ei voi kääntää kameraa pystysuunnassa.
	//lasketaan normaalivektoria käyttäen
	//siis normaalivektori tulisi laskea ennen tätä, kun tehdään muutoksia kameran suuntaan.
	double pituusn = sqrt(p->n.x * p->n.x + p->n.y * p->n.y + p->n.z * p->n.z);
	struct pisted apun = {p->n.x / pituusn, p->n.y / pituusn, p->n.z / pituusn};
	p->c.x = p->sijainti.x - p->polttopiste * apun.x;
	p->c.y = p->sijainti.y - p->polttopiste * apun.y;
	p->c.z = p->sijainti.z - p->polttopiste * apun.z;
}

void _laske_i(struct pelaaja *p)
{
	//vielä ei voi kääntää kameraa pysty/vinosuunnassa, eli i on nyt (0, 1, 0)
	//p->i.x = 0;
	//p->i.y = 1;
	//p->i.z = 0;
	struct pisted apui = {0, 1, 0};
	double s = (PISTETULO(p->n, p->n)) / (PISTETULO(p->n, p->n) - PISTETULO(p->n, apui)); //scalar
	p->i.x = p->n.x - s * p->n.x;
	p->i.y = p->n.y - s * (p->n.y - 1);
	p->i.z = p->n.z - s * p->n.z;

	double pituusi = sqrt(PISTETULO(p->i, p->i));

	p->i.x /= pituusi;
	p->i.y /= pituusi;
	p->i.z /= pituusi;


}

void _laske_e(struct pelaaja *p)
{
	//vielä ei voi kääntää kameraa pysty/vinosuunnassa
	//resoluutio on (900, 700)
	//ex = -n x i
	//ex0 = yksikkövektori ex:stä
	struct pisted ex = RISTITULO(p->n, p->i);
	double pituusex = sqrt(ex.x * ex.x + ex.y * ex.y + ex.z * ex.z);
	ex.x = ex.x / pituusex;
	ex.y = ex.y / pituusex;
	ex.z = ex.z / pituusex;

	p->e.x = p->sijainti.x + ex.x * 450 - p->i.x * 350;
	p->e.y = p->sijainti.y + ex.y * 450 - p->i.y * 350;
	p->e.z = p->sijainti.z + ex.z * 450 - p->i.z * 350;
}

void _tulosta_pelaaja(struct pelaaja *p)
{
	printf("sijanti:\t(%d, %d, %d)\n", p->sijainti.x, p->sijainti.y, p->sijainti.z);
	printf("polttop:\t(%d)\n",p->polttopiste);
	printf("paankx:\t\t(%f)\n",p->paankaantox);
	printf("paanky:\t\t(%f)\n",p->paankaantoy);
	printf("c:\t\t(%d, %d, %d)\n", p->c.x, p->c.y, p->c.z);
	printf("n:\t\t(%d, %d, %d)\n", p->n.x, p->n.y, p->n.z);
	printf("i:\t\t(%f, %f, %f)\n", p->i.x, p->i.y, p->i.z);
	printf("e:\t\t(%d, %d, %d)\n", p->e.x, p->e.y, p->e.z);


}

void _laske_pelaaja(struct pelaaja *p)
{
	_laske_n(p);
	_laske_c(p);
	_laske_i(p);
	_laske_e(p);
//	tulosta_pelaaja(p);
}

//pelaaja_suunta liikkuu vektorin n suuntaan d verran
void _pelaaja_vasen(struct pelaaja *p, int d)
{
	struct pisted apui = {0, -1, 0};
	struct pisted suunta = RISTITULO(apui, p->n);
	double pituussuunta = sqrt(suunta.x * suunta.x + suunta.y * suunta.y + suunta.z * suunta.z);
	suunta.x /= pituussuunta;
	suunta.z /= pituussuunta;
	p->sijainti.x += round(suunta.x * d);
	p->sijainti.z += round(suunta.z * d);
	_laske_pelaaja(p);
}

void _pelaaja_oikea(struct pelaaja *p,  int d)
{
	struct pisted apui = {0, 1, 0};
	struct pisted suunta = RISTITULO(apui, p->n);
	double pituussuunta = sqrt(suunta.x * suunta.x + suunta.y * suunta.y + suunta.z * suunta.z);
	suunta.x /= pituussuunta;
	suunta.z /= pituussuunta;
	p->sijainti.x += round(suunta.x * d);
	p->sijainti.z += round(suunta.z * d);
	_laske_pelaaja(p);
}

void _pelaaja_eteen(struct pelaaja *p,  int d)
{
	double pituusn = sqrt(p->n.x * p->n.x + p->n.y * p->n.y + p->n.z * p->n.z);
	struct pisted apun = {p->n.x / pituusn, p->n.y / pituusn, p->n.z / pituusn};
	p->sijainti.x += round(apun.x * d);
	p->sijainti.y += round(apun.y * d);
	p->sijainti.z += round(apun.z * d);
	_laske_pelaaja(p);
}

void _pelaaja_taakse(struct pelaaja *p,  int d)
{
	double pituusn = sqrt(p->n.x * p->n.x + p->n.y * p->n.y + p->n.z * p->n.z);
	struct pisted apun = {p->n.x / pituusn, p->n.y / pituusn, p->n.z / pituusn};
	p->sijainti.x -= round(apun.x * d);
	p->sijainti.y -= round(apun.y * d);
	p->sijainti.z -= round(apun.z * d);
	_laske_pelaaja(p);
}

void _pelaaja_ylos(struct pelaaja *p, int d)
{
	double pituusi = sqrt(p->i.x * p->i.x + p->i.y * p->i.y + p->i.z * p->i.z);
	struct pisted apui = {p->i.x / pituusi, p->i.y / pituusi, p->i.z / pituusi};
	p->sijainti.x -= round(apui.x * d);
	p->sijainti.y -= round(apui.y * d);
	p->sijainti.z -= round(apui.z * d);
	_laske_pelaaja(p);
}
void _pelaaja_alas(struct pelaaja *p, int d)
{
	double pituusi = sqrt(p->i.x * p->i.x + p->i.y * p->i.y + p->i.z * p->i.z);
	struct pisted apui = {p->i.x / pituusi, p->i.y / pituusi, p->i.z / pituusi};
	p->sijainti.x += round(apui.x * d);
	p->sijainti.y += round(apui.y * d);
	p->sijainti.z += round(apui.z * d);
	_laske_pelaaja(p);
}

void _pelaaja_kaannos_vasen(struct pelaaja *p, int d)
{
	p->paankaantox += (2 * M_PI) / 360.0 * d;
	_laske_pelaaja(p);
}

void _pelaaja_kaannos_oikea(struct pelaaja *p, int d)
{
	p->paankaantox -= (2 * M_PI) / 360.0 * d;
	_laske_pelaaja(p);
}

void _pelaaja_kaannos_alas(struct pelaaja *p, int d)
{
	p->paankaantoy -= (2 * M_PI) / 360.0 * d;
	_laske_pelaaja(p);
}

void _pelaaja_kaannos_ylos(struct pelaaja *p, int d)
{
	p->paankaantoy += (2 * M_PI) / 360.0 * d;
	_laske_pelaaja(p);
}

void _pelaaja_polttopisteplus(struct pelaaja *p, int d)
{
	p->polttopiste += d;
	_laske_pelaaja(p);
}

void _pelaaja_polttopistemiinus(struct pelaaja *p, int d)
{
	p->polttopiste -= d;
	_laske_pelaaja(p);
}
