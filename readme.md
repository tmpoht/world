# 3D world in C

This is a rehearse project made during my first study year with a fellow student.

To compile in linux use:
gcc main.c -lSDL2 -lm

Controls are wasd and rf for moving and flying and ijkluo for turning, b turns colors on/off.

![Game_footage](/game.jpg "View")