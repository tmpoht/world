#include <math.h>
#define RISTITULO(i, n) {i.y*n.z - i.z*n.y, i.z*n.x - i.x*n.z, i.x*n.y - i.y*n.x}
#define PISTETULO(p1, p2) (p1.x * p2.x + p1.y * p2.y + p1.z * p2.z)

SDL_Point _jaa_vektoreihin(struct piste jaettava, struct pisted x, struct pisted y) //Tämä funktio jakaa vektorin joka on tasossa vektorien y, x suuntaisesti
{
	//Vektorin jaettava on oltava tasossa
	//Myöskään x tai y ei saa olla nollavektoreita
	double px, py, p;

	if(jaettava.x == 0 && jaettava.y == 0 && jaettava.z == 0)
		return (SDL_Point){0,0};
	//	Funktio laskee yhtälöryhmälle
	//	a * i1 + b * j1 = h1
	//	a * i2 + b * j2 = h2
	//	a * i3 + b * j3	= h3
	//	ratkaisun siten, että se käy vaihtoehdot läpi, joissa jokin vektorin komponentti ei ole nolla.
	if(y.x != 0)
	{
		if(y.x == y.y && x.x == x.y)
		{
			p = 0.0 + y.z / y.x;
			px = (0.0 + jaettava.z - p * jaettava.x) / (jaettava.z - p * x.x);
		}
		else
		{
			p = y.y / y.x;
			px = (0.0 + jaettava.y - p * jaettava.x) / (jaettava.y - p * x.x);
		}
		py = (jaettava.x - px * x.x) / y.x;
		return (SDL_Point){(int)round(px), (int)round(py)};
	}
	//y.x == 0
	if(x.x != 0)
	{
		px = (0.0 + jaettava.x) / x.x;
		if(y.y != 0)
			py = (0.0 + jaettava.y - p * x.y) / y.y;
		else
			py = (0.0 + jaettava.z - p * x.z) / y.z;
		return (SDL_Point){(int)round(px), (int)round(py)};
	}
	// y.x == 0 && x.x == 0
	if(y.y == 0)
	{
		px = (0.0 + jaettava.y / x.y);
		py = (0.0 + jaettava.z - p * x.z) / y.z;
		return (SDL_Point){(int)round(px), (int)round(py)};
	}
	p = 0.0 + y.z / y.y;
	px = (0.0 + jaettava.z - p * jaettava.y) / (jaettava.z - p * x.y);
	return (SDL_Point){(int)round(px), (int)round(py)};
}

double _kerroin(int z)
{
	return sqrt(z)*4;
}

int _pistetulo(struct piste p1, struct piste p2)
{
	return p1.x * p2.x + p1.y * p2.y + p1.z * p2.z;
}

SDL_Point _huono_projektio1(struct piste p)
{
	// Tämä ei ole oikea projektio vaan pikainen testi.
	SDL_Point p2;
	p2.x = p.x;
	p2.y = p.y;
	return p2;
}

SDL_Point _huono_projektio2(struct piste p)
{
	// Tämä ei ole oikea projektio vaan pikainen testi.
	SDL_Point p2;
	p2.x = p.x + (p.z * 0.7);
	p2.y = p.y + (p.z * 0.7);
	return p2;
}

SDL_Point _huono_projektio3(struct piste p)
{
	//Huono projektio, mutta alkua
	SDL_Point keskip = {450, 350};
	SDL_Point p2;
	p2.x = p.x + _kerroin(p.z)*(keskip.x - p.x)/(sqrt(pow(p.x, 2) + pow(p.y,2)));
	p2.y = p.y + _kerroin(p.z)*(keskip.y - p.y)/(sqrt(pow(p.x, 2) + pow(p.y,2)));
	return p2;
}

SDL_Point _huono_projektio4(struct piste a)
{
	//Hyvän projektion alkua
	//Katso wikipediasta 3D-projektio
	//en käyttänyt asteita vaan normaalivektoria kamerantasolle.

	//seuraavaksi funktion tulisi ottaa vastaan suunta johon katsotaan, kameran sijainti jne, sekä myös onko "pää kallistunut"
	//pään kallistuma on oltava tasossa, pää kallistuu kun katsotaan ylös tai kun kallistetaan sitä kuin olalle päin.
	//pään kallistumaa kuvaa vektori i.

	struct piste c = {0, 0, -100}; //kamera
	struct piste n = {0, 0, 1}; //kameran suunta eli normaalivektori tasolle
	//Suunta voi olla vain 0, 0, 1 nyt.
	struct piste e = {-450, -350, 0}; //taso
	struct piste ac = {c.x - a.x, c.y - a.y, c.z - a.z}; //apu
	struct piste er; //suuntavektori e->r, tämä palautetaan 2D:nä tason kantavektoireilla.
	SDL_Point b;

	struct pisted i = {0,1,0}; //tasossa oleva vektori joka suuntautuu ylös, tämä tulisi ottaa vastaan argumenttina.
	struct pisted j = {i.y*n.z - i.z*n.y, i.z*n.x - i.x*n.z, i.x*n.y - i.y*n.x}; // i x n, pituus tulisi olla 1
	double jpituus = sqrt(pow(j.x,2) + pow(j.y,2) + pow(j.z,2));
	j.x = j.x/jpituus;
	j.y = j.y/jpituus;
	j.z = j.z/jpituus;

	double scalar;

	// kaikki matikka tässä projektiossa on nyt valmis.

	if(_pistetulo(n, ac) == 0) 
	{
		// kohtisuorassa polttopisteen kanssa
		// tätä ei tulisi päästää käsiteltäväksi
		scalar = 0;
	}
	else
	{
		scalar = (_pistetulo(n, e) - _pistetulo(n, a) + 0.0) / _pistetulo(n, ac);
	}

	er.x = a.x + scalar * ac.x - e.x;
	er.y = a.y + scalar * ac.y - e.y;
	er.z = a.z + scalar * ac.z - e.z;

	b = _jaa_vektoreihin(er, j, i);
	return b;
}

SDL_Point _projektio_pelaaja(struct pelaaja * pel, struct piste a)
{
	//Tämä olkoon kunnollinen ja viimeinen projektio.
	//Projektio on kuin huono_projektio4, mutta sijainnit jne. annetaan pelaaja structuressa.

	struct piste c = pel->c; //kamera
	struct piste n = pel->n; //kameran suunta eli normaalivektori tasolle
	struct piste e = pel->e; //taso
	struct piste ac = {c.x - a.x, c.y - a.y, c.z - a.z}; //apu
	struct piste er; //suuntavektori e->r, tämä palautetaan 2D:nä tason kantavektoireilla.
	SDL_Point b;

	struct pisted i = pel->i; //tasossa oleva vektori joka suuntautuu ylös ja määrää pään kallistuksen, tämä tulisi ottaa vastaan argumenttina.
	//struct pisted j = {i.y*n.z - i.z*n.y, i.z*n.x - i.x*n.z, i.x*n.y - i.y*n.x}; // i x n, pituus tulisi olla 1
	struct pisted j = RISTITULO(i, n);
	double jpituus = sqrt(pow(j.x,2) + pow(j.y,2) + pow(j.z,2));
	j.x = j.x/jpituus;
	j.y = j.y/jpituus;
	j.z = j.z/jpituus;

	double scalar;

	// kaikki matikka tässä projektiossa on nyt valmis.

	if(_pistetulo(n, ac) == 0) 
	{
		// kohtisuorassa polttopisteen kanssa
		// tätä ei tulisi päästää käsiteltäväksi
		scalar = 0;
		printf("projektio: piste-polttopiste yhdensuuntainen tason kanssa!\n");
	}
	else
	{
		scalar = (_pistetulo(n, e) - _pistetulo(n, a) + 0.0) / _pistetulo(n, ac);
	}

	er.x = a.x + scalar * ac.x - e.x;
	er.y = a.y + scalar * ac.y - e.y;
	er.z = a.z + scalar * ac.z - e.z;

	b = _jaa_vektoreihin(er, j, i);

	//printf("%d:%d\n",b.x, b.y);

	return b;
}

SDL_Point _projektio_pelaaja2(struct pelaaja * pel, struct piste a)
{
	/*
	kaikki piste/pisted
	a = projisoitava piste A
	c = kameran sijainti
	f = kameran suunta
	e = näyttötason sijainti suhteessa c:hen.
	d = apupiste eri koordinaatteihin toteutettuna.
	*/
	struct pisted c = {pel->c.x, pel->c.y, pel->c.z};
	struct pisted f = {pel->paankaantox, pel->paankaantoy, 0};
	struct pisted e = {450, 350, -100};

	SDL_Point pb;
	struct pisted b;
	struct pisted d;	//WIKIPEDIA SEURAAVA SYÖPÄ
	d.x = cos(f.y) * (sin(f.z)*(a.y - c.y) + cos(f.z)*(a.x - c.x)) - sin(f.y)*(a.z - c.z);
	d.y = sin(f.x) * (cos(f.y)*(a.z - c.z) + sin(f.y)*(sin(f.z)*(a.z - c.z) + cos(f.z)*(a.x - c.x))) + cos(f.x)*(cos(f.z)*(a.y - c.y) - sin(f.z)*(a.x - c.x));
	d.z = cos(f.x) * (cos(f.y)*(a.z - c.z) + sin(f.y)*(sin(f.z)*(a.z - c.z) + cos(f.z)*(a.x - c.x))) - sin(f.x)*(cos(f.z)*(a.y - c.y) - sin(f.z)*(a.x - c.x));

	if(d.z == 0)
	{
		printf("huutista\n");
		return (SDL_Point){0,0};
	}

	b.x = (e.z / d.z)*d.x + e.x;
	b.y = (e.z / d.z)*d.y + e.y;

	struct pisted s = {900, 700, 0};
	struct pisted r = {900, 700, 100};

	//b.x = (d.x * s.x) / (d.z * r.x) * r.z;
	//b.y = (d.y * s.y) / (d.z * r.y) * r.z;


	pb.x = (int)round(b.x);
	pb.y = (int)round(b.y);

	//printf("%d:%d\n",pb.x, pb.y);
	return pb;
}

struct pisted kaanna_koordinaatistoa(struct piste a, struct pisted c, struct pisted f)
//piste a, kameran sijainti, kulma
//kääntää koordinaatistoa kulman -f verran ja siirtää -c verran.
{
	struct pisted d;	//WIKIPEDIASTA SEURAAVA SYÖPÄ
	d.x = cos(f.y) * (sin(f.z)*(a.y - c.y) + cos(f.z)*(a.x - c.x)) - sin(f.y)*(a.z - c.z);
	d.y = sin(f.x) * (cos(f.y)*(a.z - c.z) + sin(f.y)*(sin(f.z)*(a.z - c.z) + cos(f.z)*(a.x - c.x))) + cos(f.x)*(cos(f.z)*(a.y - c.y) - sin(f.z)*(a.x - c.x));
	d.z = cos(f.x) * (cos(f.y)*(a.z - c.z) + sin(f.y)*(sin(f.z)*(a.z - c.z) + cos(f.z)*(a.x - c.x))) - sin(f.x)*(cos(f.z)*(a.y - c.y) - sin(f.z)*(a.x - c.x));
	return d;
}

SDL_Point projektio_pelaaja3(struct pelaaja2 * pel, struct piste a)
{
	/*
	kaikki piste/pisted
	a = projisoitava piste A
	c = kameran sijainti
	f = kameran suunta
	e = näyttötason sijainti suhteessa c:hen.
	d = apupiste eri koordinaatteihin toteutettuna.
	*/
	struct pisted c = {pel->c.x, pel->c.y, pel->c.z};
	struct pisted f = {pel->f.x, pel->f.y, pel->f.z};
	struct pisted e = {450, 350, 500};

	SDL_Point pb;
	struct pisted b;
	struct pisted d = kaanna_koordinaatistoa(a, c, f);

	if(d.z == 0)
	{
		printf("d.z == 0\n");
		return (SDL_Point){0,0};
	}

	b.x = (e.z / d.z)*d.x + e.x;
	b.y = (e.z / d.z)*d.y + e.y;

	struct pisted s = {900, 700, 0};
	struct pisted r = {900, 700, 100};

	//b.x = (d.x * s.x) / (d.z * r.x) * r.z;
	//b.y = (d.y * s.y) / (d.z * r.y) * r.z;


	pb.x = (int)round(b.x);
	pb.y = (int)round(b.y);

	//printf("%d:%d\n",pb.x, pb.y);
	return pb;
}

SDL_Point _huono_projektio(struct piste p)
{
	//Tämä ei ole oikea projektio
	struct pelaaja pel = _uusi_pelaaja();
	//projektio_pelaaja2(NULL, p);
	return _projektio_pelaaja2(&pel,p);
}

SDL_Point _huono_projektio_p(struct pelaaja * pel, struct piste p)
{
	//projektio_pelaaja2(NULL, p);
	return _projektio_pelaaja2(pel,p);
}

SDL_Point huono_projektio_p2(struct pelaaja2 * pel, struct piste p)
{
	//projektio_pelaaja2(NULL, p);
	return projektio_pelaaja3(pel, p);
}
