#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>


int pienin();
int suurin();
SDL_Point * nelion_pisteet();
SDL_Point * kolmion_pisteet();
void _tulosta_pisteet();
double sijoita_x_suoraan();
double sijoita_y_suoraan();
//muista huomioida erityistapaukset kuten suorat joiden k = 0 tai jotka ovat pystysuoria
void piirra();
SDL_Point suurin_x_piste();
SDL_Point suurin_y_piste();
SDL_Point pienin_x_piste();
SDL_Point pienin_y_piste();
SDL_Point maarita_suorakulma();
SDL_Point keskipiste_x_suunnassa();
SDL_Point keskipiste_y_suunnassa();
long nelion_pisteiden_lkm();
long laske_pisteet();
void piirrakolmio();

void piirra_kolmion_sivut(SDL_Renderer * rend, SDL_Point kulmat[], int vari)
{
	uint8_t vanhavari[4];

	SDL_GetRenderDrawColor(rend, &vanhavari[0], &vanhavari[1], &vanhavari[2], &vanhavari[3]);

    uint8_t red = (vari & 0xFF0000) >> 16;
    uint8_t green =  (vari & 0xFF00) >> 8;
    uint8_t blue = (vari & 0xFF);


    SDL_SetRenderDrawColor(rend, red, green, blue,255);
    SDL_RenderDrawLine(rend,kulmat[0].x, kulmat[0].y, kulmat[1].x, kulmat[1].y);
    SDL_RenderDrawLine(rend,kulmat[1].x, kulmat[1].y, kulmat[2].x, kulmat[2].y);
    SDL_RenderDrawLine(rend,kulmat[2].x, kulmat[2].y, kulmat[0].x, kulmat[0].y);

	SDL_SetRenderDrawColor(rend, vanhavari[0],vanhavari[1],vanhavari[2],vanhavari[3]);
}






void piirrakolmio(SDL_Renderer * rend, SDL_Point kulmat[], int vari) //renderöijä, kolme pistettä eli piirrettävän kolmion kulmat, vari hexadesimaalina
{                                                                    //vari esim. ff00dd
	
    SDL_Point * pisteet =  kolmion_pisteet(kulmat[0], kulmat[1], kulmat[2]);

	uint8_t vanhavari[4];

	SDL_GetRenderDrawColor(rend, &vanhavari[0], &vanhavari[1], &vanhavari[2], &vanhavari[3]);

    uint8_t red = (vari & 0xFF0000) >> 16;
    uint8_t green =  (vari & 0xFF00) >> 8;
    uint8_t blue = (vari & 0xFF);


	long lkm = laske_pisteet(pisteet);
    SDL_SetRenderDrawColor(rend, red, green, blue,255);
	SDL_RenderDrawPoints(rend, pisteet, lkm);

	SDL_SetRenderDrawColor(rend, vanhavari[0],vanhavari[1],vanhavari[2],vanhavari[3]);
	free(pisteet);
}
SDL_Point * kolmion_pisteet(SDL_Point a, SDL_Point b, SDL_Point c)
{
     long max_lkm = nelion_pisteiden_lkm(a,b,c);
    /*if (max_lkm > 100000)
    {
        SDL_Point * taulukko = (SDL_Point*)malloc(sizeof(SDL_Point));
        taulukko[0].x = -1;
        taulukko[0].y = -1;

        return taulukko;
    }*/

    SDL_Point * taulukko2 = nelion_pisteet(a,b,c);
   
    //tulosta_pisteet(taulukko2);
    
    SDL_Point * taulukko3 = (SDL_Point*)malloc((max_lkm + 1)* sizeof(SDL_Point));
    

    SDL_Point korkein = suurin_y_piste(a,b,c);
    SDL_Point matalin = pienin_y_piste(a,b,c);
    SDL_Point vasen = pienin_x_piste(a,b,c);
    SDL_Point oikea = suurin_x_piste(a,b,c);
    SDL_Point keskix = keskipiste_x_suunnassa(a,b,c);
    SDL_Point keskiy = keskipiste_y_suunnassa(a,b,c);
    SDL_Point suorakulma = maarita_suorakulma(a,b,c);



    int onko_suorakulmainen = 0;
    int tapaus = 0;
    int onko_samoja_pisteita = 0;

    
    long i = 0;
    long j = 0;
    if ((a.x == b.x && a.y == b.y)||(b.x == c.x && b.y == c.y)||(a.x == c.x && a.y == c.y))
    {
        onko_samoja_pisteita = 1;
    }
    //jos kaksi pistettä on samoja, kolmio on kapea viiva ja sitä ei tarvitse piirtää 
    if(onko_samoja_pisteita == 0)
    {
        //suorakulmaiset kolmiot
        if (vasen.x == -1 && matalin.x == -1)
        {
            onko_suorakulmainen = 1;
            tapaus = 9;
        }
        else if (vasen.x == -1 && korkein.x == -1)
        {
            onko_suorakulmainen = 1;
            tapaus = 10;
        }
        else if (oikea.x == -1 && matalin.x == -1)
        {
            onko_suorakulmainen = 1;
            tapaus = 11;
        }
        else if(oikea.x == -1 && korkein.x == -1)
        {
            onko_suorakulmainen = 1;
            tapaus = 12;
        }

        

        //apumuuttuja, kertoo onko kolmio sellainen että jotain seuraavista pisteistä ei voi määritellä
        int maaritys = 0;
        if (matalin.x == -1||korkein.x == -1||vasen.x == -1||oikea.x == -1)
        {
            maaritys = 1;
        }


        //tapausmahdollisuudet ei suorakulmaisilla
        
        if (onko_suorakulmainen == 0)
        {
            if (maaritys == 0)
            {
                //korkein on vasen ja matalin on oikea, keskipiste suoran yläpuolella
                if (korkein.x == vasen.x && korkein.y == vasen.y && oikea.x == matalin.x && oikea.y == matalin.y && keskiy.y > sijoita_x_suoraan(keskiy.x,korkein, matalin))
                {
                    tapaus = 1;
                }
                //korkein on vasen ja matalin on oikea, keskipiste suoran alapuolella
                else if (korkein.x == vasen.x && korkein.y == vasen.y && oikea.x == matalin.x && oikea.y == matalin.y && keskiy.y < sijoita_x_suoraan(keskiy.x,korkein, matalin))
                {
                    tapaus = 2;
                }
                //korkein on vasen ja vaakasuunnassa keskimmäinen on matalin
                else if(korkein.x == vasen.x && korkein.y == vasen.y && keskix.x == matalin.x && keskix.y == matalin.y)
                {
                    tapaus = 3;
                }
                //korkein on oikea ja vaakasuunnassa keskimmäinen on matalin
                else if(korkein.x == oikea.x && korkein.y == oikea.y && keskix.x == matalin.x && keskix.y == matalin.y)
                {
                    tapaus = 4;
                }
                //korkein on oikea ja vasen on matalin ja keskipiste suoran yläpuolella
                else if(korkein.x == oikea.x && korkein.y == oikea.y && vasen.x == matalin.x && vasen.y == matalin.y && keskiy.y > sijoita_x_suoraan(keskiy.x,korkein, matalin))
                {
                    tapaus = 5;
                }
                //korkein on oikea ja vasen on matalin ja keskipiste suoran alapuolella
                else if(korkein.x == oikea.x && korkein.y == oikea.y && vasen.x == matalin.x && vasen.y == matalin.y && keskiy.y < sijoita_x_suoraan(keskiy.x,korkein, matalin))
                {
                    tapaus = 6;
                }
                //korkein on keskellä ja vasen on matalin
                else if(keskix.x == korkein.x && keskix.y == korkein.y && vasen.x == matalin.x && vasen.y == matalin.y)
                {
                    tapaus = 7;
                }//korkein on keskellä ja oikea on matalin
                else if(keskix.x == korkein.x && keskix.y == korkein.y && oikea.x == matalin.x && oikea.y == matalin.y)
                {
                    tapaus = 8;
                }
            }
            else
                {
                if(matalin.x == -1) //matalinta pistettä ei pystytä määrittelemään
                {
                    tapaus = 13;
                }
                else if(korkein.x == -1) //korkeinta pistettä ei kyetä määrittelemään
                {
                    tapaus = 14;
                }
                else if(oikea.x == -1) //oikeaa pistettä ei kyetä määrittelemään
                {
                    tapaus = 15;
                }    
                else if(vasen.x == -1) //vasenta pistettä ei kyetä määrittelemään
                {
                    tapaus = 16;
                }
            }
        
        }


        for(i = 0; taulukko2[i].x != -1; i++)
        {
            int ehto = 0;
            switch(tapaus)
            {
                case 1:
                    if(taulukko2[i].y >= (int) sijoita_x_suoraan(taulukko2[i].x,vasen,oikea) && taulukko2[i].y <= (int) sijoita_x_suoraan(taulukko2[i].x,vasen, keskiy) && taulukko2[i].x <= (int) sijoita_y_suoraan(taulukko2[i].y,oikea,keskiy))
                    {
                        ehto = 1;
                    }
                    break;
                case 2:
                    if(taulukko2[i].y <= (int) sijoita_x_suoraan(taulukko2[i].x,vasen,oikea) && taulukko2[i].y >= (int) sijoita_x_suoraan(taulukko2[i].x,oikea, keskiy) && taulukko2[i].x >= (int) sijoita_y_suoraan(taulukko2[i].y,vasen,keskiy))
                    {
                        ehto = 1;
                    }
                    break;
                case 3:
                    if(taulukko2[i].y <= (int) sijoita_x_suoraan(taulukko2[i].x,vasen,oikea) && taulukko2[i].y >= (int) sijoita_x_suoraan(taulukko2[i].x,vasen,keskix) && taulukko2[i].x <= (int) sijoita_y_suoraan(taulukko2[i].y,keskix,oikea))
                    {
                        ehto = 1;
                    }
                    break;
                case 4:
                    if(taulukko2[i].y <= (int) sijoita_x_suoraan(taulukko2[i].x,vasen,oikea) && taulukko2[i].y >= (int) sijoita_x_suoraan(taulukko2[i].x,oikea,keskix) && taulukko2[i].x >= (int) sijoita_y_suoraan(taulukko2[i].y,keskix,vasen))
                    {
                        ehto = 1;
                    }
                    break;
                case 5:
                    if(taulukko2[i].y >= (int) sijoita_x_suoraan(taulukko2[i].x,vasen,oikea) && taulukko2[i].y <= (int) sijoita_x_suoraan(taulukko2[i].x,oikea, keskiy) && taulukko2[i].x >= (int) sijoita_y_suoraan(taulukko2[i].y,vasen,keskiy))
                    {
                        ehto = 1;
                    }
                    break;
                case 6:
                    if(taulukko2[i].y <= (int) sijoita_x_suoraan(taulukko2[i].x,vasen,oikea) && taulukko2[i].y >= (int) sijoita_x_suoraan(taulukko2[i].x,vasen, keskiy) && taulukko2[i].x <= (int) sijoita_y_suoraan(taulukko2[i].y,oikea,keskiy))
                    {
                        ehto = 1;
                    }
                    break;

                case 7:
                    if(taulukko2[i].y >= (int) sijoita_x_suoraan(taulukko2[i].x,vasen, oikea) && taulukko2[i].y <= (int) sijoita_x_suoraan(taulukko2[i].x,vasen, keskix) && taulukko2[i].x <= (int) sijoita_y_suoraan(taulukko2[i].y,oikea, keskix))
                    {
                        ehto = 1;
                    }
                    break;
                case 8:
                    if(taulukko2[i].y >= (int) sijoita_x_suoraan(taulukko2[i].x,vasen, oikea) && taulukko2[i].y <= (int) sijoita_x_suoraan(taulukko2[i].x,oikea, keskix) && taulukko2[i].x >= (int) sijoita_y_suoraan(taulukko2[i].y,vasen, keskix))
                    {
                        ehto = 1;
                    }
                    break;
                case 9: //korkein, oikea ja suorakulma
                    if(taulukko2[i].y >= suorakulma.y && taulukko2[i].x >= suorakulma.x && taulukko2[i].y <= (int) sijoita_x_suoraan(taulukko2[i].x,korkein,oikea))
                    {
                        ehto = 1;
                    }
                    break;
                case 10: //matalin, oikea ja suorakulma
                    if(taulukko2[i].y <= suorakulma.y && taulukko2[i].x >= suorakulma.x && taulukko2[i].y >= (int) sijoita_x_suoraan(taulukko2[i].x,matalin,oikea))
                    {
                        ehto = 1;
                    }
                    break;
                case 11: //korkein, vasen ja suorakulma
                    if(taulukko2[i].y >= suorakulma.y && taulukko2[i].x <= suorakulma.x && taulukko2[i].y <= (int) sijoita_x_suoraan(taulukko2[i].x,korkein,vasen))
                    {
                        ehto = 1;
                    }
                    break;
                case 12: //matalin, vasen ja suorakulma
                    if(taulukko2[i].y <= suorakulma.y && taulukko2[i].x <= suorakulma.x && taulukko2[i].y >= (int) sijoita_x_suoraan(taulukko2[i].x,matalin, vasen))
                    {
                        ehto = 1;
                    }
                    break;
                case 13: //matalinta pistettä ei pystytä määrittelemään
                    if (keskix.x == korkein.x && taulukko2[i].y <= (int) sijoita_x_suoraan(taulukko2[i].x, korkein, vasen) && taulukko2[i].x <= (int) sijoita_y_suoraan(taulukko2[i].y, korkein, oikea) && taulukko2[i].y >= oikea.y)
                    {
                        ehto = 1;
                    }
                    else if(vasen.x == korkein.x && taulukko2[i].y <= (int) sijoita_x_suoraan(taulukko2[i].x, vasen, oikea) && taulukko2[i].x >= (int) sijoita_y_suoraan(taulukko2[i].y, vasen, keskix) && taulukko2[i].y >= oikea.y)
                    {
                        ehto = 1;
                    }
                    else if(oikea.x == korkein.x && taulukko2[i].y <= (int) sijoita_x_suoraan(taulukko2[i].x, vasen, oikea) && taulukko2[i].x <= (int) sijoita_y_suoraan(taulukko2[i].y, oikea, keskix) && taulukko2[i].y >= vasen.y)
                    {
                        ehto = 1;
                    }
                    break;
                case 14: //korkeinta pistettä ei pystytä määrittelemään
                    if (keskix.x == matalin.x && taulukko2[i].y >= (int) sijoita_x_suoraan(taulukko2[i].x, matalin, vasen) && taulukko2[i].x <= (int) sijoita_y_suoraan(taulukko2[i].y, matalin, oikea) && taulukko2[i].y <= oikea.y)
                    {
                        ehto = 1; 
                    }
                    else if (vasen.x == matalin.x && taulukko2[i].y >= (int) sijoita_x_suoraan(taulukko2[i].x, vasen, oikea) && taulukko2[i].x >= (int) sijoita_y_suoraan(taulukko2[i].y, vasen, keskix) && taulukko2[i].y <= oikea.y)
                    {
                        ehto = 1;
                    }
                    else if (oikea.x == matalin.x && taulukko2[i].y >= (int) sijoita_x_suoraan(taulukko2[i].x, vasen, oikea) && taulukko2[i].x <= (int) sijoita_y_suoraan(taulukko2[i].y, oikea, keskix) && taulukko2[i].y <= vasen.y)
                    {
                        ehto = 1;
                    }
                    break;
                case 15: //oikeaa pistettä ei kyetä määrittelemään
                    if (keskiy.x == vasen.x && taulukko2[i].y >= (int) sijoita_x_suoraan(taulukko2[i].x, vasen, matalin) && taulukko2[i].x >= (int) sijoita_y_suoraan(taulukko2[i].y, vasen, korkein) && taulukko2[i].x <= korkein.x)
                    {
                        ehto = 1;
                    }
                    else if (korkein.x == vasen.x && taulukko2[i].y <= (int) sijoita_x_suoraan(taulukko2[i].x, vasen, keskiy) && taulukko2[i].x >= (int) sijoita_y_suoraan(taulukko2[i].y, vasen, matalin) && taulukko2[i].x <= matalin.x)
                    {
                        ehto = 1;
                    }
                    else if (matalin.x == vasen.x && taulukko2[i].y >= (int) sijoita_x_suoraan(taulukko2[i].x, vasen, keskiy) && taulukko2[i].x >= (int) sijoita_y_suoraan(taulukko2[i].y, vasen, korkein) && taulukko2[i].x <= korkein.x)
                    {
                        ehto = 1;
                    }
                    break;
                case 16://vasenta pistettä ei kyetä määrittelemään
                    if (keskiy.x == oikea.x && taulukko2[i].y >= (int) sijoita_x_suoraan(taulukko2[i].x, matalin, oikea) && taulukko2[i].x <= (int) sijoita_y_suoraan(taulukko2[i].y, oikea, korkein) && taulukko2[i].x >= matalin.x)
                    {
                        ehto = 1;
                    }
                    else if (korkein.x == oikea.x && taulukko2[i].y <= (int) sijoita_x_suoraan(taulukko2[i].x, keskiy,korkein) && taulukko2[i].x <= (int) sijoita_y_suoraan(taulukko2[i].y, matalin, korkein) && taulukko2[i].x >= matalin.x)
                    {
                        ehto = 1;
                    }
                    else if (matalin.x == oikea.x && taulukko2[i].y >= (int) sijoita_x_suoraan(taulukko2[i].x, keskiy, matalin) && taulukko2[i].x <= (int) sijoita_y_suoraan(taulukko2[i].y, korkein, oikea) && taulukko2[i].x >= korkein.x)
                    {
                        ehto = 1;
                    }
                    break;
            }

            if (ehto == 1)
            {
                taulukko3[j].x = taulukko2[i].x;
                taulukko3[j].y = taulukko2[i].y;
                j = j + 1;
            }  
        } 
    }
    taulukko3[j].x = -1;
    taulukko3[j].y = -1;
    free(taulukko2);
    return taulukko3;
}
double sijoita_x_suoraan(int x_sijoitettava, SDL_Point a1, SDL_Point a2)
{
    if (a1.y == a2.y)
    {
        return (double)a1.y;
    }
    double k = ((double)a1.y - (double)a2.y)/((double)a1.x - (double)a2.x); //kulmakerroin
    double c = (double)a1.y - (k * (double)a1.x); //vakio
    double y = (k * (double)x_sijoitettava )+ c;
    return y;

}
double sijoita_y_suoraan(int y_sijoitettava,  SDL_Point b1, SDL_Point b2)
{
    if (b1.x == b2.x)
    {
        return (double)b1.x;
    }
    double k =  ((double)b1.y - (double)b2.y)/((double)b1.x - (double)b2.x); //kulmakerroin
    double c = (double)b1.y -( k * (double)b1.x); //vakio
    double x = ((double)y_sijoitettava - c)/k;
    return x;
}




SDL_Point * nelion_pisteet(SDL_Point a,SDL_Point b,SDL_Point c)
{
    int alku_x = pienin(a.x,b.x,c.x);
    int loppu_x = suurin(a.x,b.x,c.x);
    int alku_y = pienin(a.y,b.y,c.y);
    int loppu_y = suurin(a.y,b.y,c.y);

    int leveys = loppu_x - alku_x;
    int korkeus = loppu_y - alku_y;
    long maara = (long)korkeus * (long)leveys;


    SDL_Point * taulukko = (SDL_Point*)malloc((maara+1) * sizeof(SDL_Point));
    
    long i = 0;
    int piste_x = alku_x;
    int piste_y = alku_y;

    for(i = 0; i < maara; i = i + 1)
    {

        taulukko[i].x = piste_x;
        taulukko[i].y = piste_y;
        
        piste_x = piste_x + 1;
       
        if(piste_x == leveys + alku_x)
        {
            piste_y = piste_y + 1;
            piste_x = alku_x;
        }
        
    } 

    taulukko[maara].x = -1;
    taulukko[maara].y = -1;

    return taulukko;
}
SDL_Point maarita_suorakulma(SDL_Point a, SDL_Point b, SDL_Point c)
{
    SDL_Point sk;
    sk.x = -1;
    sk.y = -1;

    if ((a.x == b.x && a.y == c.y)||(a.x == c.x && a.y == b.y))
    {
        sk = a;
    }
    else if ((b.x == c.x && b.y == a.y)||(b.x == a.x && b.y == c.y))
    {
        sk = b;
    }
    else if ((c.x == a.x && c.y == b.y)||(c.x == b.x && c.y == a.y))
    {
        sk = c;
    }
    
    return sk;

}

int suurin(int a, int b, int c)
{
    int max = -1;

    if((a >= b)&&(a >= c))
    {
        max = a;
    }
    else if ((b >= a)&&(b >= c))
    {
        max = b;
    }
    else
    {
        max = c;
    }
    return max;

}
int pienin(int a, int b, int c)
{
    int min =-1;

    if((a <= b)&&(a <= c))
    {
        min = a;
    }
    else if ((b <= a)&&(b <= c))
    {
        min = b;
    }
    else
    {
        min = c;
    }
    return min;
    
}
SDL_Point pienin_x_piste(SDL_Point a, SDL_Point b, SDL_Point c)
{
    SDL_Point min;
    min.x = -1;
    min.y = -1;

    if((a.x < b.x)&&(a.x < c.x))
    {
        min = a;
    }
    else if ((b.x < a.x)&&(b.x < c.x))
    {
        min = b;
    }
    else if ((c.x < a.x)&&(c.x < b.x))
    {
        min = c;
    }
    return min;
    
}

SDL_Point pienin_y_piste(SDL_Point a, SDL_Point b, SDL_Point c)
{
    SDL_Point min;
    min.x = -1;
    min.y = -1;

    if((a.y < b.y)&&(a.y < c.y))
    {
        min = a;
    }
    else if ((b.y < a.y)&&(b.y < c.y))
    {
        min = b;
    }
    else if ((c.y < a.y)&&(c.y < b.y))
    {
        min = c;
    }
    return min;
    
}
SDL_Point suurin_y_piste(SDL_Point a, SDL_Point b, SDL_Point c)
{
    SDL_Point max;
    max.x = -1;
    max.y = -1;

    if((a.y > b.y)&&(a.y > c.y))
    {
        max = a;
    }
    else if ((b.y > a.y)&&(b.y > c.y))
    {
        max = b;
    }
    else if((c.y > a.y)&&(c.y > b.y))
    {
        max = c;
    }
    return max;

}
SDL_Point suurin_x_piste(SDL_Point a, SDL_Point b, SDL_Point c)
{
    SDL_Point max;
    max.x = -1;
    max.y = -1;

    if((a.x > b.x)&&(a.x > c.x))
    {
        max = a;
    }
    else if ((b.x > a.x)&&(b.x > c.x))
    {
        max = b;
    }
    else if ((c.x > a.x)&&(c.x > b.x))
    {
        max = c;
    }
    return max;

}
SDL_Point keskipiste_y_suunnassa(SDL_Point a, SDL_Point b, SDL_Point c)
{
    SDL_Point mid;
    mid.x = -1;
    mid.y = -1;

    if((b.y > c.y && a.y > b.y)||(b.y > a.y && c.y > b.y))
    {
        mid = b;
    }
    else if ((a.y > c.y && b.y > a.y)||(c.y > a.y && a.y > b.y))
    {
        mid = a;
    }
    else if ((c.y > b.y && a.y > c.y)||(c.y > a.y && b.y > c.y))
    {
        mid = c;
    }
    return mid;

}
SDL_Point keskipiste_x_suunnassa(SDL_Point a, SDL_Point b, SDL_Point c)
{
    SDL_Point mid;
    mid.x = -1;
    mid.y = -1;

    if((b.x > c.x && a.x > b.x)||(b.x > a.x && c.x > b.x))
    {
        mid = b;
    }
    else if ((a.x > c.x && b.x > a.x)||(a.x > b.x && c.x > a.x))
    {
        mid = a;
    }
    else if ((c.x > a.x && b.x > c.x)||(c.x > b.x && a.x > c.x))
    {
        mid = c;
    }
    return mid;

}

void _tulosta_pisteet(SDL_Point * taulu1)
{
   long i = 0;
    
    for(i = 0;taulu1[i].x != -1 ; i++)
    {
        printf("x = %d, y = %d\n", taulu1[i].x, taulu1[i].y);
        
    }
}
long laske_pisteet(SDL_Point * taulu1)
{
    long i = 0;
    
    for(i = 0;taulu1[i].x != -1 ; i++)
    ;
    return i;
}
long nelion_pisteiden_lkm(SDL_Point a, SDL_Point b, SDL_Point c)
{
    int alku_x = pienin(a.x,b.x,c.x);
    int loppu_x = suurin(a.x,b.x,c.x);
    int alku_y = pienin(a.y,b.y,c.y);
    int loppu_y = suurin(a.y,b.y,c.y);

    int leveys = loppu_x - alku_x;
    int korkeus = loppu_y - alku_y;
    long maara = (long)korkeus * (long)leveys;
    return maara;
}
