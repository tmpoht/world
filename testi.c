//#define SDL_MAIN_HANDLED
#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include "kolmion_pisteet.c"

void piirrakolmio();


int main(int argc, char* args[])
{
	SDL_Window *window = NULL;
	SDL_Renderer *renderer= NULL;
	
	
	SDL_Init(SDL_INIT_VIDEO);
	
	window = SDL_CreateWindow("Ikkuna",SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 500,300,0);
	renderer = SDL_CreateRenderer(window,-1,0);
	
	if(window == NULL||renderer == NULL)
	{
		printf("Virhe: %s\n", SDL_GetError());
		return 1;
	}
	SDL_SetRenderDrawColor(renderer,0,0,0,255);
	SDL_RenderClear(renderer);

	//1
	//2
	SDL_Point b1 = {0,100};
	SDL_Point b2 = {100,0};
	SDL_Point b3 = {10,50};
	//3
	SDL_Point c1 = {130,100};
	SDL_Point c2 = {100,200};
	SDL_Point c3 = {200,130};
	//4
	SDL_Point d1 = {200,130};
	SDL_Point d2 = {230,100};
	SDL_Point d3 = {300,200};
	//5
	SDL_Point e1 = {300,100};
	SDL_Point e2 = {400,200};
	SDL_Point e3 = {310,150};
	//6
	SDL_Point f1 = {300,100};
	SDL_Point f2 = {400,200};
	SDL_Point f3 = {390,150};
	//7
	SDL_Point g1 = {400,100};
	SDL_Point g2 = {500,150};
	SDL_Point g3 = {450,200};
	//8
	SDL_Point h1 = {600,100};
	SDL_Point h2 = {500,150};
	SDL_Point h3 = {550,200};
	//9
	SDL_Point i1 = {300,300};
	SDL_Point i2 = {300,400};
	SDL_Point i3 = {400,300};
	//10
	SDL_Point j1 = {500,500};
	SDL_Point j2 = {400,500};
	SDL_Point j3 = {400,400};
	//11
	SDL_Point k1 = {600,650};
	SDL_Point k2 = {650,600};
	SDL_Point k3 = {600,600};
	//12
	SDL_Point l1 = {50,50};
	SDL_Point l2 = {10,50};
	SDL_Point l3 = {50,10};
	//13a
	SDL_Point m1 = {350,350};
	SDL_Point m2 = {375,400};
	SDL_Point m3 = {400,350};
	//13b
	SDL_Point n1 = {340,400};
	SDL_Point n2 = {375,350};
	SDL_Point n3 = {400,350};
	//13c
	SDL_Point o1 = {350,350};
	SDL_Point o2 = {375,350};
	SDL_Point o3 = {410,400};
	//14a
	SDL_Point p1 = {350,350};
	SDL_Point p2 = {375,300};
	SDL_Point p3 = {400,350};
	//14b
	SDL_Point q1 = {375,350};
	SDL_Point q2 = {340,300};
	SDL_Point q3 = {400,350};
	//14c
	SDL_Point r1 = {350,350};
	SDL_Point r2 = {410,300};
	SDL_Point r3 = {375,350};
	//15a
	SDL_Point s1 = {400,550};
	SDL_Point s2 = {500,500};
	SDL_Point s3 = {500,600};
	//15b
	SDL_Point t1 = {400,490};
	SDL_Point t2 = {500,500};
	SDL_Point t3 = {500,600};
	//15c
	SDL_Point u1 = {400,610};
	SDL_Point u2 = {500,500};
	SDL_Point u3 = {500,600};
	//16a
	SDL_Point v1 = {600,550};
	SDL_Point v2 = {500,600};
	SDL_Point v3 = {500,500};
	//16b
	SDL_Point x1 = {600,490};
	SDL_Point x2 = {500,600};
	SDL_Point x3 = {500,500};
	//16c
	SDL_Point y1 = {600,610};
	SDL_Point y2 = {500,600};
	SDL_Point y3 = {500,500};

	SDL_Point kolmio2[3] = {b1,b2,b3};
	SDL_Point kolmio3[3] = {c1,c2,c3};
	SDL_Point kolmio4[3] = {d1,d2,d3};
	SDL_Point kolmio5[3] = {e1,e2,e3};
	SDL_Point kolmio6[3] = {f1,f2,f3};
	SDL_Point kolmio7[3] = {g1,g2,g3};
	SDL_Point kolmio8[3] = {h1,h2,h3};
	SDL_Point kolmio9[3] = {i1,i2,i3};
	SDL_Point kolmio10[3] = {j1,j2,j3};
	SDL_Point kolmio11[3] = {k1,k2,k3};
	SDL_Point kolmio12[3] = {l1,l2,l3};
	SDL_Point kolmio13[3] = {m1,m2,m3};
	SDL_Point kolmio14[3] = {n1,n2,n3};
	SDL_Point kolmio15[3] = {o1,o2,o3};
	SDL_Point kolmio16[3] = {p1,p2,p3};
	SDL_Point kolmio17[3] = {q1,q2,q3};
	SDL_Point kolmio18[3] = {r1,r2,r3};
	SDL_Point kolmio19[3] = {s1,s2,s3};
	SDL_Point kolmio20[3] = {t1,t2,t3};
	SDL_Point kolmio21[3] = {u1,u2,u3};
	SDL_Point kolmio22[3] = {v1,v2,v3};
	SDL_Point kolmio23[3] = {x1,x2,x3};
	SDL_Point kolmio24[3] = {y1,y2,y3};


	int varia = 0x0000ff;
	int varib = 0xff0000;
	int varic = 0x00ff00;
	int varid = 0xff00ff;
	int varie = 0xffff00;
	int varif = 0xffffff;
	int varig = 0x00ffff;
	int varih = 0x880000;
	int varii = 0x008800;
	int varij = 0x000088;
	int varik = 0x880088;
	int varil = 0x888800;
	int varim = 0x888888;
	int varin = 0x008888;
	int vario = 0x440000;
	int varip = 0x004400;
	int variq = 0x000044;
	int varir = 0x004444;
	int varis = 0x440044;
	int varit = 0x444444;
	int variu = 0xaa0000;
	int variv = 0x00aa00;
	int varix = 0x0000aa;
	int variy = 0x00aaaa;



	for(int i = 0; i < 40; i = i + 10)
	{
	
		for (int j = 0; j < 40; j = j + 10)
		{
					
			SDL_Point a1 = {0,0};
			SDL_Point a2 = {30,30};
			SDL_Point a3 = {i,j};
			SDL_Point kolmio1[3] = {a1,a2,a3};

			SDL_RenderClear(renderer);

			piirrakolmio(renderer,kolmio1,varia);
			printf("i: %d, j: %d\n",i,j);
			SDL_RenderPresent(renderer);
			
			SDL_Delay(1000);
		}
		
	}



/*	piirrakolmio(renderer,kolmio2,varib);
	piirrakolmio(renderer,kolmio3,varic);
	piirrakolmio(renderer,kolmio4,varid);
	piirrakolmio(renderer,kolmio5,varie);
	piirrakolmio(renderer,kolmio6,varif);
	piirrakolmio(renderer,kolmio7,varig);
	piirrakolmio(renderer,kolmio8,varih);
	piirrakolmio(renderer,kolmio9,varii);
	piirrakolmio(renderer,kolmio10,varij);
	piirrakolmio(renderer,kolmio11,varik);
	piirrakolmio(renderer,kolmio12,varil);
	piirrakolmio(renderer,kolmio13,varim);
	piirrakolmio(renderer,kolmio14,varin);
	piirrakolmio(renderer,kolmio15,vario);
	piirrakolmio(renderer,kolmio16,varip);
	piirrakolmio(renderer,kolmio17,variq);
	piirrakolmio(renderer,kolmio18,varir);
	piirrakolmio(renderer,kolmio19,varis);
	piirrakolmio(renderer,kolmio20,varit);
	piirrakolmio(renderer,kolmio21,variu);
	piirrakolmio(renderer,kolmio22,variv);
	piirrakolmio(renderer,kolmio23,varix);
	piirrakolmio(renderer,kolmio24,variy);*/


/* 	struct piste * pallo1 = pallon_pisteet(kp,30,10,10);

	tulosta_pallon_pisteet(pallo1);

	free(pallo1);

 */
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
	return 0;
}

