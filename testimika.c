#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "kolmion_pisteet.c"
#include "objektit.c"
#include "pelaaja.c"
#include "projektio.c"
#include "maailma.c"
#include "lasku.c"

int main()
{
	int i = 0;
	srand(time(NULL));
	SDL_Point ulkopc[10];
	SDL_Point sisapc[10];

	for(i = 0; i < 10; i++)
	{
		ulkopc[i].x = -(rand() % 900);
		ulkopc[i].y = rand() % 700;
	}
	for(i = 0; i < 10; i++)
	{
		sisapc[i].x = (rand() % 900);
		sisapc[i].y = 700 + (rand() % 700);
	}

	SDL_Window *window = NULL;
	SDL_Renderer *renderer= NULL;
	
	SDL_Init(SDL_INIT_VIDEO);
	
	window = SDL_CreateWindow("Ikkuna",SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 900,700,0);
	renderer = SDL_CreateRenderer(window,-1,SDL_RENDERER_ACCELERATED);
	
	if(window == NULL||renderer == NULL)
	{
		printf("Virhe: %s\n", SDL_GetError());
		return 1;
	}

	SDL_SetRenderDrawColor(renderer,0,0,0,255);
	SDL_RenderClear(renderer);

	//piirra normiviivat
	SDL_SetRenderDrawColor(renderer, 255, 0, 0, SDL_ALPHA_OPAQUE);
	for(i = 0; i < 10; i++)
	{
		SDL_RenderDrawLine(renderer, ulkopc[i].x, ulkopc[i].y, sisapc[i].x, sisapc[i].y);
	}


	SDL_SetRenderDrawColor(renderer, 0, 255, 0, SDL_ALPHA_OPAQUE);
	for(i = 0; i < 10; i++)
	{
		SDL_RenderDrawLine(renderer, katkaisekolmionviiva(ulkopc[i], sisapc[i]).x, katkaisekolmionviiva(ulkopc[i], sisapc[i]).y, katkaisekolmionviiva(sisapc[i], ulkopc[i]).x, katkaisekolmionviiva(sisapc[i], ulkopc[i]).y);
	}

	SDL_RenderPresent(renderer);

	SDL_Delay(1000);

	//exit
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
	exit(0);
}
