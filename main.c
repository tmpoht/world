#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include "kolmion_pisteet.c"
#include "objektit.c"
#include "pelaaja.c"
#include "projektio.c"
#include "maailma.c"
#include "lasku.c"


struct painetutnappulat
{
	int w, a, s, d, r, f; //liikkuminen
	int i, j, k, l, u, o; //kameran liikuttaminen

};
//mainloop, missä tapahtuu eventtien luku ja piirto ja lasku

int mainloop(SDL_Window *window, SDL_Renderer *renderer)
{
	Uint32 gameticks = SDL_GetTicks();
	struct painetutnappulat painetut = {0,0,0,0,0,0,0,0,0,0,0,0};
	int quit = 0;
	long i;
	int muutos = 1;
	SDL_Event event;
	struct maailma * maa = luo_maailma7();
	struct d2kolmiolista piirrettavat;
	//struct pelaaja pel = uusi_pelaaja();
	struct pelaaja2 pel2 = uusi_pelaaja2();
	//tulosta_pelaaja(&pel);
	/*laske_n(&pel);
	laske_c(&pel);
	laske_i(&pel);
	laske_e(&pel);*/
	int leveys;
	int korkeus;
	int piirtoviivoin = 1;

	SDL_GetWindowSize(window,&leveys,&korkeus);

	//1
	SDL_Point a1 = {0,0};
	SDL_Point a2 = {leveys,0};
	SDL_Point a3 = {0,korkeus/2};
	SDL_Point a4 = {leveys,korkeus/2};


	SDL_Point taivas1[3] = {a1,a2,a3};
	SDL_Point taivas2[3] = {a2,a3,a4};


	//tulosta_pelaaja(&pel);

	printf("paina wasdrf liikutellaksesi pelaajaa ja ijkluo liikuttaaksesi kuvakulmaa.\n");


	while(!quit)
	{
	//tänne looppiin piirtofunktio
	//tänne looppiin laskufunktio jne.
	
	if(muutos == 1)
	{
		SDL_RenderClear(renderer);

		//piirrakolmio(renderer, taivas1, 0x3333ff);
		//piirrakolmio(renderer, taivas2, 0x3333ff);
		piirrettavat = laske_maailma(maa, &pel2);
		for(i = 0; i < piirrettavat.maara; i++)
		{
			if(piirtoviivoin == 0)
				piirrakolmio(renderer, piirrettavat.pisteet[i],piirrettavat.pisteet[i][3].x);
			else
				piirra_kolmion_sivut(renderer, piirrettavat.pisteet[i], 0xffffff);
		}

		free(piirrettavat.pisteet);
		//piirra listan kolmiot

		//piirto loppuu

		SDL_RenderPresent(renderer);
	}
	for(;(i = SDL_PollEvent(&event)) == 1 && event.type != SDL_QUIT && event.type != SDL_KEYDOWN && event.type != SDL_KEYUP;); // tyhjentää event jonon turhasta
	if(i == 1) //eventtien luku ja mahdollinen lasku
	{
	switch(event.type)
	{
	case SDL_QUIT:
		quit = 1;
		printf("QUIT!\n");
		break;
	case SDL_KEYDOWN:
		switch(event.key.keysym.sym)
		{
		case SDLK_w: //liikkuminen
			painetut.w = 1;
			break;
		case SDLK_a:
			painetut.a = 1;
			break;
		case SDLK_s:
			painetut.s = 1;
			break;
		case SDLK_d:
			painetut.d = 1;
			break;
		case SDLK_r:
			painetut.r = 1;
			break;
		case SDLK_f:
			painetut.f = 1;
			break;
		case SDLK_i:
			painetut.i = 1;
			break;
		case SDLK_k:
			painetut.k = 1;
			break;
		case SDLK_u: //kameran kääntö
			painetut.u = 1;
			break;
		case SDLK_o: //kameran kääntö
			painetut.o = 1;
			break;
		case SDLK_l: //päänkääntö
			painetut.l = 1;
			break;
		case SDLK_j:
			painetut.j = 1;
			break;
		case SDLK_b:
			if(piirtoviivoin == 1)
				piirtoviivoin = 0;
			else
				piirtoviivoin = 1;
			break;
		}
		SDL_FlushEvent(SDL_KEYDOWN);
		break;
	case SDL_KEYUP:
		switch(event.key.keysym.sym)
		{
		case SDLK_w: //liikkuminen
			painetut.w = 0;
			break;
		case SDLK_a:
			painetut.a = 0;
			break;
		case SDLK_s:
			painetut.s = 0;
			break;
		case SDLK_d:
			painetut.d = 0;
			break;
		case SDLK_r:
			painetut.r = 0;
			break;
		case SDLK_f:
			painetut.f = 0;
			break;
		case SDLK_i:
			painetut.i = 0;
			break;
		case SDLK_k:
			painetut.k = 0;
			break;
		case SDLK_u: //kameran kääntö
			painetut.u = 0;
			break;
		case SDLK_o: //kameran kääntö
			painetut.o = 0;
			break;
		case SDLK_l: //päänkääntö
			painetut.l = 0;
			break;
		case SDLK_j:
			painetut.j = 0;
			break;
		}
		break;
	}
	}
	if(muutos == 1)
	{
		if(painetut.w == 1) pelaaja2_y(&pel2, 10);
		if(painetut.a == 1) pelaaja2_x(&pel2, 10);
		if(painetut.s == 1) pelaaja2_y(&pel2, -10);
		if(painetut.d == 1) pelaaja2_x(&pel2, -10);
		if(painetut.r == 1) pelaaja2_z(&pel2, 10);
		if(painetut.f == 1) pelaaja2_z(&pel2, -10);
		if(painetut.i == 1) pelaaja2_kaannos_ylos(&pel2, 1);
		if(painetut.k == 1) pelaaja2_kaannos_alas(&pel2, 1);
		if(painetut.j == 1) pelaaja2_kaannos_paa_oikea(&pel2, 1);
		if(painetut.l == 1) pelaaja2_kaannos_paa_vasen(&pel2, 1);
		if(painetut.u == 1) pelaaja2_kaannos_vasen(&pel2, 1);
		if(painetut.o == 1) pelaaja2_kaannos_oikea(&pel2, 1);
	}
	muutos = 0;
	if(SDL_GetTicks() - gameticks > 10)
	{
		double tickspersecond = 1000.0/(SDL_GetTicks() - gameticks +0.0);
		printf("ticks per second: %f\n",tickspersecond);
		gameticks = SDL_GetTicks();
		muutos = 1;
	}
	}
	return 0;
}

int main(int argc, char *argv[])
{
	SDL_Window *window = NULL;
	SDL_Renderer *renderer= NULL;
	
	SDL_Init(SDL_INIT_VIDEO);
	
	window = SDL_CreateWindow("Ikkuna",SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 900,700,0);
	renderer = SDL_CreateRenderer(window,-1,SDL_RENDERER_ACCELERATED);
	
	if(window == NULL||renderer == NULL)
	{
		printf("Virhe: %s\n", SDL_GetError());
		return 1;
	}

	SDL_SetRenderDrawColor(renderer,33,33,255,255);

	// perusjutut kunnes mainlooppiin tästä

	mainloop(window, renderer);

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
	exit(0);
}

