#include <stdlib.h>
#include <math.h>
/*
 * kaikki objektit koostuvat vain kolmiotasoista, jotka koostuvat kolmesta 3d pisteestä.
 *
 *
 */
struct kolmiodata
{
    float normaali[3];
    float kulma1[3];
    float kulma2[3];
    float kulma3[3];
    short attribuutti;
};
struct piste
{
	int x, y, z;
};

struct pisted	//tämä olkoon apuna laskemisessa.
{
	double x, y, z;
};


struct taso //kolmion koordinaatit ja väri
{
	struct piste pisteet[3];
	int vari;	// #rrggbb
};

struct objekti
{
	struct taso *tasot;
	int maara; // tasojen määrä listassa
	char *nimi;
};


void siirra_objektia(struct objekti * obj,int x, int y, int z)
// siirtää objektia (x,y,z) vektorin suuntaan
{
	int i, j;
	struct taso *t = obj->tasot;

	for(i = 0; i < obj->maara; i++)
	{
		for(j = 0; j < 3; j++)
		{
			t->pisteet[j].x += x;
			t->pisteet[j].y += y;
			t->pisteet[j].z += z;
		}
		t += 1;
	}
}

void muuta_objektin_kokoa(struct objekti * obj, double d)
// pyöristää lähimpään kokonaislukuun.
{
	int i, j;
	struct taso *t = obj->tasot;

	for(i = 0; i < obj->maara; i++)
	{
		for(j = 0; j < 3; j++)
		{
			t->pisteet[j].x = (int)round(t->pisteet[j].x * d);
			t->pisteet[j].y = (int)round(t->pisteet[j].y * d);
			t->pisteet[j].z = (int)round(t->pisteet[j].z * d);
		}
		t += 1;
	}
}


struct objekti * uusi_tiedostosta(char tiedostonimi[], int tasovari){

	struct objekti * obj = NULL;
	struct taso * t = NULL;
	char * n = tiedostonimi;
	int i;

	FILE *fp = fopen(tiedostonimi,"rb");
    char otsake[80];
    unsigned int lkm;
	fread(&otsake, sizeof(otsake), 1, fp);
    fread(&lkm, sizeof(lkm),1,fp);

    struct kolmiodata kolmiot[lkm];

    for(int i = 0; i<lkm; i++)
    {
        fread(&(kolmiot[i].normaali), sizeof(kolmiot[i].normaali),1,fp);
        fread(&(kolmiot[i].kulma1), sizeof(kolmiot[i].kulma1),1,fp);
        fread(&(kolmiot[i].kulma2), sizeof(kolmiot[i].kulma2),1,fp);
        fread(&(kolmiot[i].kulma3), sizeof(kolmiot[i].kulma3),1,fp);
        fread(&(kolmiot[i].attribuutti), sizeof(kolmiot[i].attribuutti),1,fp);
    }

    fclose(fp);

	obj = (struct objekti *)malloc(sizeof(struct objekti));
	obj->tasot = (struct taso *)malloc(sizeof(struct taso) * lkm);
	obj->nimi = (char *)malloc(sizeof(n));

	n = obj->nimi;
	t = obj->tasot;
	obj->maara = lkm;

	for(int j = 0; j <lkm; j++)
	{
		struct piste apupiste1 ={(int)kolmiot[j].kulma1[0], (int)kolmiot[j].kulma1[1], (int)kolmiot[j].kulma1[2]};
		struct piste apupiste2 ={(int)kolmiot[j].kulma2[0], (int)kolmiot[j].kulma2[1], (int)kolmiot[j].kulma2[2]};
		struct piste apupiste3 ={(int)kolmiot[j].kulma3[0], (int)kolmiot[j].kulma3[1], (int)kolmiot[j].kulma3[2]};
		(t+j)->vari = tasovari;
		(t+j)->pisteet[0] = apupiste1;
		(t+j)->pisteet[1] = apupiste2;
		(t+j)->pisteet[2] = apupiste3;
	}

	return obj;

}


//luo kuution jonka särmän pituus on d ja koord (0,0,0)
//kuutio tehdään kokonaan dynaamisesti
struct objekti * _uusi_kuutio(int d)
{
	struct objekti * obj = NULL;
	struct taso * t = NULL;
	char *n = NULL;
	int i;

	//malloc kaikki tarvittava
	obj = (struct objekti *)malloc(sizeof(struct objekti));
	obj->tasot = (struct taso *)malloc(sizeof(struct taso) * 12);
	obj->nimi = (char *)malloc(sizeof(char) * 7);

	n = obj->nimi;
	*n = 'k';
	*(n + 1) = 'u';
	*(n + 2) = 'u';
	*(n + 3) = 't';
	*(n + 4) = 'i';
	*(n + 5) = 'o';
	*(n + 6) = '\0';

	t = obj->tasot;
	obj->maara = 12;

	//värit tasoille
	for(i = 0; i < 12; i++)
		(t + i)->vari = 0x50f050;

	//kolmion pisteet, huono tapa.

	t->pisteet[0] = (struct piste){0, 0, 0};
	t->pisteet[1] = (struct piste){d, 0, 0};
	t->pisteet[2] = (struct piste){0, 0, d};

	(t + 1)->pisteet[0] = (struct piste){d, 0, d};
	(t + 1)->pisteet[1] = (struct piste){d, 0, 0};
	(t + 1)->pisteet[2] = (struct piste){0, 0, d};

	(t + 2)->pisteet[0] = (struct piste){0, d, 0};
	(t + 2)->pisteet[1] = (struct piste){d, d, 0};
	(t + 2)->pisteet[2] = (struct piste){0, d, d};

	(t + 3)->pisteet[0] = (struct piste){d, d, d};
	(t + 3)->pisteet[1] = (struct piste){d, d, 0};
	(t + 3)->pisteet[2] = (struct piste){0, d, d};


	(t + 4)->pisteet[0] = (struct piste){0, 0, 0};
	(t + 4)->pisteet[1] = (struct piste){0, d, 0};
	(t + 4)->pisteet[2] = (struct piste){0, 0, d};

	(t + 5)->pisteet[0] = (struct piste){0, 0, d};
	(t + 5)->pisteet[1] = (struct piste){0, d, 0};
	(t + 5)->pisteet[2] = (struct piste){0, d, d};

	(t + 6)->pisteet[0] = (struct piste){d, 0, 0};
	(t + 6)->pisteet[1] = (struct piste){d, d, 0};
	(t + 6)->pisteet[2] = (struct piste){d, 0, d};

	(t + 7)->pisteet[0] = (struct piste){d, 0, d};
	(t + 7)->pisteet[1] = (struct piste){d, d, 0};
	(t + 7)->pisteet[2] = (struct piste){d, d, d};

	(t + 8)->pisteet[0] = (struct piste){0, 0, 0};
	(t + 8)->pisteet[1] = (struct piste){d, 0, 0};
	(t + 8)->pisteet[2] = (struct piste){0, d, 0};

	(t + 9)->pisteet[0] = (struct piste){d, d, 0};
	(t + 9)->pisteet[1] = (struct piste){d, 0, 0};
	(t + 9)->pisteet[2] = (struct piste){0, d, 0};

	(t + 10)->pisteet[0] = (struct piste){0, 0, d};
	(t + 10)->pisteet[1] = (struct piste){d, 0, d};
	(t + 10)->pisteet[2] = (struct piste){0, d, d};

	(t + 11)->pisteet[0] = (struct piste){d, d, d};
	(t + 11)->pisteet[1] = (struct piste){d, 0, d};
	(t + 11)->pisteet[2] = (struct piste){0, d, d};

	return obj;
}

struct objekti * uusi_slab(int d)
{
	struct objekti * obj = NULL;
	struct taso * t = NULL;
	char *n = NULL;
	int i;

	//malloc kaikki tarvittava
	obj = (struct objekti *)malloc(sizeof(struct objekti));
	obj->tasot = (struct taso *)malloc(sizeof(struct taso) * 2);
	obj->nimi = (char *)malloc(sizeof(char) * 5);

	n = obj->nimi;
	*n = 't';
	*(n + 1) = 'a';
	*(n + 2) = 's';
	*(n + 3) = 'o';
	*(n + 4) = '\0';

	t = obj->tasot;
	obj->maara = 2;

	//värit tasoille
	for(i = 0; i < 2; i++)
		(t + i)->vari = 0x425140;

	//kolmion pisteet, huono tapa.

	t->pisteet[0] = (struct piste){0, 0, 0};
	t->pisteet[1] = (struct piste){d, 0, 0};
	t->pisteet[2] = (struct piste){0, d, 0};

	(t + 1)->pisteet[0] = (struct piste){d, 0, 0};
	(t + 1)->pisteet[1] = (struct piste){0, d, 0};
	(t + 1)->pisteet[2] = (struct piste){d, d, 0};

	return obj;
}

void poista_objekti(struct objekti * obj) // poistaa dynaamisesti määritellyn objektin
{
	free(obj->nimi);
	free(obj->tasot);
	free(obj);
}
